﻿using FleetManagerApp.BTInterfaces;
using FleetManagerApp.EventsArgs;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DevicesListPage : ContentPage
    {
        private IBTManager _btManager;

        public event EventHandler<DeviceSelectedEventArgs> OnDeviceSelected;

        public DevicesListPage()
        {
            InitializeComponent();

            Init();
        }

        public void Init()
        {
            _btManager = DependencyService.Get<IBTManager>();

            InitListOfDevices();
        }

        public void InitListOfDevices()
        {
            listDevices.ItemTapped += ListDevices_ItemTapped;
            var list = _btManager.GetPairedDevices();
            listDevices.ItemsSource = list;
        }

        private void ListDevices_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var item = e.Item as IBTDevice;
            if (item != null)
            {
                DeviceSelectedEventArgs args = new DeviceSelectedEventArgs();
                args.Device = item;
                OnDeviceSelected?.Invoke(this, args);
            }
        }
    }
}
