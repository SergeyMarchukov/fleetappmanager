﻿
using FleetManagerApp.BTInterfaces;
using FleetManagerApp.DTCOSmartLinkLib;
using FleetManagerApp.DTCOSmartLinkLib.Commands;
using FleetManagerApp.DTCOSmartLinkLib.Enums;
using FleetManagerApp.Enums;
using FleetManagerApp.Tool;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MemberStatePage : ContentPage
    {
        private string Tag = "DEVELOPMENT";

        private IBTManager _btManager;
        private ILogger _log;

        public MemberStatePage()
        {
            InitializeComponent();

            Init();
        }
        
        public void Init()
        {
            Title = "Registering Member State";

            _btManager = DependencyService.Get<IBTManager>();
            _log = DependencyService.Get<ILogger>();

            listEnum.ItemTapped += ListEnum_ItemTapped;
            listEnum.ItemsSource = Enum.GetNames(typeof(MemberStateCodeEnum));

            ReadMemberState();
        }

        private async void ListEnum_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                MemberStateCodeEnum countryCode = (MemberStateCodeEnum)Enum.Parse(typeof(MemberStateCodeEnum), e.Item.ToString());
                SmartLinkCmdRegisteringMemberState cmd = new SmartLinkCmdRegisteringMemberState((byte)AppDataStorage.Get(SmartLinkConstants.SESSION_TOKEN), new byte[] { (byte)countryCode });
                Task.Factory.StartNew(() => SendMessageToSmartLink(cmd.GetWriteMessage(), OperationCallback));
            }
            catch (Exception ex)
            {
                DisplayAlert("", ex.Message, "Ok");
            }
        }

        private void ReadMemberState()
        {
            SmartLinkCmdRegisteringMemberState cmd = new SmartLinkCmdRegisteringMemberState((byte)AppDataStorage.Get(SmartLinkConstants.SESSION_TOKEN));
            Task.Factory.StartNew(() => SendMessageToSmartLink(cmd.GetReadMessage(), OperationCallback));
        }

        public async void SendMessageToSmartLink(byte[] sendData, Action<SmartLinkOperationResult> callback)
        {
            if (!AppDataStorage.Exists(SmartLinkConstants.SESSION_TOKEN) || !_btManager.IsConnected)
            {
                callback?.Invoke(new SmartLinkOperationResult().SetError(BluetoothConnectionErrorEnum.BLUETOOTH_CONNECTION_LOST));
                return;
            }

            if (sendData != null && sendData.Length > 0)
            {
                try
                {
                    await _btManager.WriteAsync(sendData);
                    _log.Info("SENDED BYTES: [ " + BitConverter.ToString(sendData) + " ]");

                    SmartLinkOperationResult operationResult = await _btManager.ReadAsync();
                    _log.Info(operationResult.Message);

                    callback?.Invoke(operationResult);
                }
                catch (Exception ex)
                {
                    callback?.Invoke(new SmartLinkOperationResult().SetError(ex.Message));
                }
            }
            else
            {
                callback?.Invoke(new SmartLinkOperationResult().SetError("Sended Data can not be empty"));
            }
        }

        private void OperationCallback(SmartLinkOperationResult result)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                lblResult.Text = result.Message;

                if (!result.IsOk || result.IsNegativeResponse)
                {
                    _log.Error(result.Message);
                    DisplayAlert("", result.Message, "Ok");
                }
            });
        }
    }
}
