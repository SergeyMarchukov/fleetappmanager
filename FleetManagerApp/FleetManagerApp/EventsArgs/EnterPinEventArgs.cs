﻿using FleetManagerApp.DTCOSmartLinkLib;
using System;

/// <summary>
/// Created by Sergey M. 22.03.17
/// </summary>
namespace FleetManagerApp.EventsArgs
{
    public class EnterPinEventArgs : EventArgs
    {
        public SmartLinkPIN Pin { get; set; }
    }
}
