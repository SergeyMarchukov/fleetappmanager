﻿using FleetManagerApp.BTInterfaces;
using System;

namespace FleetManagerApp.EventsArgs
{
    public class DeviceSelectedEventArgs : EventArgs
    {
        public IBTDevice Device { get; set; }
    }
}
