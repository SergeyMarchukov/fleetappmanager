﻿using FleetManagerApp.BTInterfaces;
using FleetManagerApp.DTCOSmartLinkLib;
using FleetManagerApp.DTCOSmartLinkLib.Commands;
using FleetManagerApp.EventsArgs;
using FleetManagerApp.Tool;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConnectPage : ContentPage
    {
        // 144 157969
        // 488 271500
        private IBTManager _btManager;
        //private IBTDevice _btDevice;
        private ILogger _log;
        //private int mSessionToken = 0x00;

        public ConnectPage()
        {
            InitializeComponent();

            Init();
        }

        public void Init()
        {
            Title = "Connect To SmartLink";

            AppDataStorage.Set(SmartLinkConstants.SESSION_TOKEN, 0x00);

            _log = LoggerSingleton.LoggerInstance;
            _btManager = BTManagerSingleton.BTManagerInstance;

            //btnSettings.IsEnabled = false;

            btnConnectToSmartlink.Clicked += BtnConnectToSmartlink_Clicked;
            btnLicenseArgeement.Clicked += BtnLicenseArgeement_Clicked;
            btnSettings.Clicked += BtnSettings_Clicked;
            
        }

        private async void BtnSettings_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ConfigurationParamsPage());
        }

        private void BtnConnectToSmartlink_Clicked(object sender, EventArgs e)
        {
            if (!_btManager.IsEnable)
            {
                _btManager.IntentEnableBluetooth();
            }
            else
            {
                if (Device.OS == TargetPlatform.Android)
                {
                    var devicesListPage = new DevicesListPage();
                    devicesListPage.OnDeviceSelected += OnDeviceSelected;
                    Navigation.PushModalAsync(devicesListPage, true);
                }                
            }            
        }

        private async void BtnLicenseArgeement_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LicenseAgreement());
        }

        public async void OnDeviceSelected(object sender, DeviceSelectedEventArgs e)
        {
            _log.Info("DEVICE SELECTED");

            if (e.Device != null)
            {
                _btManager.CreateSocket(e.Device);

                await Navigation.PopModalAsync();

                if (Device.OS == TargetPlatform.Android)
                {
                    EnterPinPage enterPinPage = new EnterPinPage();
                    enterPinPage.SmartLinkName = e.Device.Name;
                    enterPinPage.OnEnteredPin += OnEnteredPin;
                    await Navigation.PushModalAsync(enterPinPage);
                }
            }
        }

        private async void OnEnteredPin(object sender, EnterPinEventArgs e)
        {
            _log.Info("PIN ENTERED");

            if (e.Pin != null)
            {
                try
                {
                    await Navigation.PopModalAsync(true);

                    await _btManager.ConnectToRemoteDevice();

                    SmartLinkCmdPin cmdPin = new SmartLinkCmdPin(Convert.ToByte(AppDataStorage.Get(SmartLinkConstants.SESSION_TOKEN)), e.Pin.ByteValue.ToArray());

                    Task.Factory.StartNew(() => SendMessageToSmartLink(cmdPin.Message, ConnectionCallback));

                }
                catch (Exception ex)
                {
                    _log.Error("CONNECT PAGE (ENTERED PIN) ERROR: " + ex.Message);
                }
            }
        }

        public async void SendMessageToSmartLink(byte[] buffer, Action<string> callback)
        {
            try
            {
                await _btManager.WriteAsync(buffer);

                SmartLinkOperationResult operationResult = await _btManager.ReadAsync();
                if (operationResult.IsOk)
                {
                    AppDataStorage.Set(SmartLinkConstants.SESSION_TOKEN, operationResult.SmartLinkMessage.SessionToken);

                    Device.BeginInvokeOnMainThread(() => btnSettings.IsEnabled = true);
                }

                callback?.Invoke(operationResult.Message);
            }
            catch (Exception ex)
            {
                callback?.Invoke(ex.Message);
            }
        }

        //TODO: removeor rewrite
        public void ConnectionCallback(string msg)
        {
            Device.BeginInvokeOnMainThread(() => DisplayAlert("", msg, "Ok"));
        }
    }
}
