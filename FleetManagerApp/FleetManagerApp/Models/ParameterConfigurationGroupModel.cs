﻿using System.Collections.Generic;

namespace FleetManagerApp.Models
{
    public class ParameterConfigurationGroupModel : List<ParameterConfigurationModel>
    {
        public string Title { get; set; }
        
        public List<ParameterConfigurationModel> ParamsList { get; set; }
    }
}
