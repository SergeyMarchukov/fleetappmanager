﻿using FleetManagerApp.Enums;
using System;

namespace FleetManagerApp.Models
{
    public class ParameterConfigurationModel
    {
        public string Title { get; set; }

        public ParamPermissionsEnum Permissions { get; set; }

        public string Value { get; set; }

        public Type TypeOfCmd { get; set; }

        public Type TypeOfPage { get; set; }
    }
}
