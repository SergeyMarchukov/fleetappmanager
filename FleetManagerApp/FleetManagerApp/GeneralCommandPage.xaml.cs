﻿using FleetManagerApp.BTInterfaces;
using FleetManagerApp.DTCOSmartLinkLib;
using FleetManagerApp.DTCOSmartLinkLib.Commands;
using FleetManagerApp.Enums;
using FleetManagerApp.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GeneralCommandPage : ContentPage
    {
        private string Tag = "DEVELOPMENT";

        private IBTManager _btManager;
        private ILogger _log;
        
        public Type CommandType;

        public GeneralCommandPage()
        {
            InitializeComponent();

            Init();
        }

        private void Init()
        {
            Title = "General Command Page";

            _btManager = DependencyService.Get<IBTManager>();
            _log = DependencyService.Get<ILogger>();

            ReadDataCommand();
        }

        private void ReadDataCommand()
        {
            try
            {
                CommandType = (Type)AppDataStorage.Get("cmdType");
                SmartLinkCmdBase cmd = (SmartLinkCmdBase)Activator.CreateInstance(CommandType, (byte)AppDataStorage.Get(SmartLinkConstants.SESSION_TOKEN));
                Task.Factory.StartNew(() => SendMessageToSmartLink(cmd.GetReadMessage(), OperationCallback));
            }
            catch (Exception ex)
            {
                DisplayAlert("", ex.Message, "Ok");
            }
        }

        private async void SendMessageToSmartLink(byte[] sendData, Action<SmartLinkOperationResult> callback)
        {
            if (!AppDataStorage.Exists(SmartLinkConstants.SESSION_TOKEN) || !_btManager.IsConnected)
            {
                callback?.Invoke(new SmartLinkOperationResult().SetError(BluetoothConnectionErrorEnum.BLUETOOTH_CONNECTION_LOST));
                return;
            }

            if (sendData != null && sendData.Length > 0)
            {
                try
                {                    
                    Device.BeginInvokeOnMainThread(() => lblSended.Text = BitConverter.ToString(sendData));

                    _log.Info("SENDED BYTES: [ " + BitConverter.ToString(sendData) + " ]");

                    await _btManager.WriteAsync(sendData);

                    SmartLinkOperationResult operationResult = await _btManager.ReadAsync();

                    _log.Info(operationResult.Message);

                    callback?.Invoke(operationResult);
                }
                catch (Exception ex)
                {
                    callback?.Invoke(new SmartLinkOperationResult().SetError(ex.Message));
                }
            }
            else
            {
                callback?.Invoke(new SmartLinkOperationResult().SetError("Sended Data can not be empty"));
            }
        }

        private void OperationCallback(SmartLinkOperationResult result)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (result.SmartLinkMessage != null)
                { lblReceivedBytes.Text = BitConverter.ToString(result.SmartLinkMessage.Packet.ToArray()); }
                lblReceivedData.Text = result.Message;

                if (!result.IsOk || result.IsNegativeResponse)
                {
                    _log.Error(result.Message);
                    lblReceivedException.Text = result.Message;
                    DisplayAlert("", result.Message, "Ok");
                }
            });
        }
    }
}
