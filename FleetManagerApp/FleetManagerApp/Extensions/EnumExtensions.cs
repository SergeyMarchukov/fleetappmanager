using FleetManagerApp.Attributes;
using System;
using System.Reflection;

namespace FleetManagerApp.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            if (value == null) { return ""; }

            var type = value.GetType();
            var field = type.GetRuntimeField(value.ToString());

            var description = field.GetCustomAttribute(typeof(EnumDescriptionAttribute)) as EnumDescriptionAttribute;
            return description == null ? value.ToString() : description.Text;
        }
    }
}