﻿using System;

namespace FleetManagerApp.Attributes
{
    public class EnumDescriptionAttribute : Attribute
    {
        public string Text { get; set; }
    }
}
