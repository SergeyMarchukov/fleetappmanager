﻿using FleetManagerApp.DTCOSmartLinkLib;
using FleetManagerApp.EventsArgs;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterPinPage : ContentPage
    {
        public string SmartLinkName { get; set; }
        public event EventHandler<EnterPinEventArgs> OnEnteredPin;

        public EnterPinPage()
        {
            InitializeComponent();

            Init();
        }

        public void Init()
        {
            lblPinFor.Text += SmartLinkName;
            btnSetPin.Clicked += BtnSetPin_Clicked;
            
            txtPin.Focus();
        }

        private void BtnSetPin_Clicked(object sender, EventArgs e)
        {
            SmartLinkPIN slPin = new SmartLinkPIN(txtPin.Text);
            if (!string.IsNullOrEmpty(txtPin.Text) && slPin.Valid)
            {
                EnterPinEventArgs args = new EnterPinEventArgs();
                args.Pin = slPin;
                OnEnteredPin?.Invoke(this, args);
            }
            else
            {
                DisplayAlert("", "The Pin can consist only of digits", "Ok");
            }
        }
    }
}
