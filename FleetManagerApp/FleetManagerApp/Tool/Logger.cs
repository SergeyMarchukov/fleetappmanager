﻿using Xamarin.Forms;

namespace FleetManagerApp.Tool
{
    public interface ILogger
    {
        void Error(string msg);

        void Info(string msg);
    }

    public static class LoggerSingleton
    {
        private static ILogger _loggerInstance;

        public static ILogger LoggerInstance { get { return _loggerInstance; } }

        static LoggerSingleton()
        {
            _loggerInstance = DependencyService.Get<ILogger>();
        }
    }
}
