﻿
using Xamarin.Forms;

namespace FleetManagerApp.Tool
{
    public static class AppDataStorage
    {
        public static void Set(string key, object value)
        {
            if (Application.Current.Properties.Keys.Contains(key))
            { Application.Current.Properties[key] = value; }
            else
            { Application.Current.Properties.Add(key, value); }
        }

        public static object Get(string key)
        {
            return Application.Current.Properties[key];
        }

        public static bool Exists(string key)
        {
            return Application.Current.Properties.Keys.Contains(key)
                && Application.Current.Properties[key] != null;
        }
    }
}
