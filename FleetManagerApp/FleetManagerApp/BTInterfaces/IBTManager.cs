﻿using FleetManagerApp.DTCOSmartLinkLib;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FleetManagerApp.BTInterfaces
{
    public interface IBTManager : IEquatable<IBTManager>
    {

        bool IsEnable { get; }

        bool IsConnected { get; }

        bool IsDiscovering { get; }

        void IntentEnableBluetooth();

        ObservableCollection<IBTDevice> GetPairedDevices();

        void CreateSocket(IBTDevice device);

        Task ConnectToRemoteDevice();
        
        Task<SmartLinkOperationResult> WriteAsync(byte[] buffer);

        Task<SmartLinkOperationResult> ReadAsync();
        
    }
}
