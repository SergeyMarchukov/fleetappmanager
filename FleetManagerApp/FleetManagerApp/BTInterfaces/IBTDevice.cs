﻿using System;

namespace FleetManagerApp.BTInterfaces
{
    public interface IBTDevice : IEquatable<IBTDevice>
    {
        string Name { get; }

        string Address { get; }

        object NativeBTDevice { get; }
    }
}
