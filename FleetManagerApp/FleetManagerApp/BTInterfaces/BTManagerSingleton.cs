﻿using Xamarin.Forms;

namespace FleetManagerApp.BTInterfaces
{
    public static class BTManagerSingleton
    {
        private static IBTManager _btManagerInstance;

        public static IBTManager BTManagerInstance { get { return _btManagerInstance; } }

        static BTManagerSingleton()
        {
            _btManagerInstance = DependencyService.Get<IBTManager>();
        }
    }
}
