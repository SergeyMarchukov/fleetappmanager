﻿using FleetManagerApp.Attributes;
/// <summary>
/// Created by Sergey M. 20.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib.Enums
{
    public enum SmartLinkNegativeResponseEnum
    {
        [EnumDescription(Text = "Internal problem")]
        GeneralReject = 0x10,
        [EnumDescription(Text = "Service not supported")]
        ServiceNotSupported = 0x11,
        [EnumDescription(Text = "Subfunction not supported")]
        SubFunctionNotSupported = 0x12,
        [EnumDescription(Text = "Incorrect message length or Invalid format")]
        IncorrectMessageLengthOrInvalidFormat = 0x13,
        [EnumDescription(Text = "Busy repeat request")]
        BusyRepeatRequest = 0x21,
        [EnumDescription(Text = "Conditions not correct")]
        ConditionsNotCorrect = 0x22,
        [EnumDescription(Text = "Request sequence error")]
        RequestSequenceError = 0x24,
        [EnumDescription(Text = "Request out of range")]
        RequestOutOfRange = 0x31,
        [EnumDescription(Text = "Security access denied")]
        SecurityAccessDenied = 0x33,
        [EnumDescription(Text = "Invalid key")]
        InvalidKey = 0x35,
        [EnumDescription(Text = "Exceeded number of attempts")]
        ExceededNumberOfAttempts = 0x36,
        [EnumDescription(Text = "General programming failure")]
        GeneralProgrammingFailure = 0x72,
        [EnumDescription(Text = "Request correctly received response pending")]
        RequestCorrectlyReceivedResponsePending = 0x78,
        [EnumDescription(Text = "Service not supported in active session")]
        ServiceNotSupportedInActiveSession = 0x7F,
        [EnumDescription(Text = "TLV Objects are not consistent")]
        TLVObjectsAreNotConsistent = 0xFA
    }
}
