﻿using FleetManagerApp.Attributes;
/// <summary>
/// Created by Sergey M. 20.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib.Enums
{
    public enum SmartLinkErrorCodeEnum
    {
        [EnumDescription(Text = "NO ERROR")]
        NO_ERROR = 0,
        [EnumDescription(Text = "INVALID PIN / WRONG PIN")]
        INVALID_PIN = 0xF3,
        [EnumDescription(Text = "SMARTLINK IN UPDATE MODE")]
        SMARTLINK_IN_UPDATE_MODE = 0xF4,
        [EnumDescription(Text = "NAK")]
        NAK = 0xF5,
        [EnumDescription(Text = "WRONG ID")]
        WRONG_ID = 0xF6,
        [EnumDescription(Text = "RESEND BLOCK")]
        RESEND_BLOCK = 0xF7,
        OTHER_SESSION_ACTIVE = 0xF8,
        [EnumDescription(Text = "TOKEN ERROR")]
        TOKEN_ERROR = 0xF9,
        [EnumDescription(Text = "INVALID TOKEN ID (SESSION ID)")]
        INVALID_TOKEN_ID = 0xFA,
        [EnumDescription(Text = "TRANSMISSION ERROR (Irc)")]
        TRANSMISSION_ERROR = 0xFB,
        [EnumDescription(Text = "BUFFER OVERFLOW")]
        BUFFER_OVERFLOW = 0xFC,
        [EnumDescription(Text = "NO DATA")]
        NO_DATA = 0xFD,
        [EnumDescription(Text = "SMART LINK BUSY")]
        SMART_LINK_BUSY = 0xFE,
        [EnumDescription(Text = "COMMAND TIMEOUT")]
        COMMAND_TIMEOUT = 0xFF        
    }
}
