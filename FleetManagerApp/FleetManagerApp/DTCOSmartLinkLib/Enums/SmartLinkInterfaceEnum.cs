/// <summary>
/// Created by Sergey M. 19.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib.Enums
{
    /// <summary>
    /// Smart Link Interfaces
    /// </summary>
    public enum SmartLinkInterfaceEnum
    {
        KLINE = 0x00,
        DOWNLOAD = 0x01,
        CONFIGURATION = 0x02,
        UPGRADE = 0x03,
        SESSION_HANDLING = 0x04,
        ERROR = 0x05
    }
}