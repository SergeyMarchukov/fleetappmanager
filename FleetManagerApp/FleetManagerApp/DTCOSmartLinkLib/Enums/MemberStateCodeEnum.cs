﻿using FleetManagerApp.Attributes;

namespace FleetManagerApp.DTCOSmartLinkLib.Enums
{
    public enum MemberStateCodeEnum
    {
        [EnumDescription(Text = "No information available")]
        EMPTY = 00,
        [EnumDescription(Text = "Albania (AL)")]
        Albania = 0x02,
        [EnumDescription(Text = "Andorra (AND)")]
        Andorra = 0x03,
        [EnumDescription(Text = "Armenia ARM")]
        Armenia = 0x04,
        [EnumDescription(Text = "Austria (A)")]
        Austria = 0x01,
        [EnumDescription(Text = "Azerbaijan (AZ)")]
        Azerbaijan = 0x05,
        [EnumDescription(Text = "Belarus (BY)")]
        Belarus = 0x09,
        [EnumDescription(Text = "Belgium (B)")]
        Belgium = 0x06,
        [EnumDescription(Text = "Bosnia Herzegovina (BIH)")]
        Bosnia_Herzegovina = 0x08,
        [EnumDescription(Text = "Bulgaria (BG)")]
        Bulgaria = 0x07,
        [EnumDescription(Text = "Croatia (HR)")]
        Croatia = 0x19,
        [EnumDescription(Text = "Cyprus (CY)")]
        Cyprus = 0x0B,
        [EnumDescription(Text = "Czech Republic( CZ)")]
        Czech_Republic = 0x0C,
        [EnumDescription(Text = "Denmark (DK)")]
        Denmark = 0x0E,
        [EnumDescription(Text = "Estonia (EST)")]
        Estonia = 0x10,
        //[EnumDescription(Text = "")]
        //Faroe Islands   Faroe Islands   FR	0x14,   No longer in use since 1996. – Replaced by FO.
        [EnumDescription(Text = "Finland (FIN)")]
        Finland = 0x12,
        [EnumDescription(Text = "France (F)")]
        France = 0x11,
        [EnumDescription(Text = "Georgia (GE)")]
        Georgia = 0x16,
        [EnumDescription(Text = "Germany (D)")]
        Germany = 0x0D,
        [EnumDescription(Text = "Greece (GR)")]
        Greece = 0x17,
        [EnumDescription(Text = "Hungary (H)")]
        Hungary = 0x18,
        [EnumDescription(Text = "Iceland (IS)")]
        Iceland = 0x1C,
        [EnumDescription(Text = "Ireland (IRL)")]
        Ireland = 0x1B,
        [EnumDescription(Text = "Italy (I)")]
        Italy = 0x1A,
        [EnumDescription(Text = "Kazakhstan (KZ)")]
        Kazakhstan = 0x1D,
        [EnumDescription(Text = "Latvia (LV)")]
        Latvia = 0x20,
        [EnumDescription(Text = "Liechtenstein (FL)")]
        Liechtenstein = 0x13,
        [EnumDescription(Text = "Lithuania (LT)")]
        Lithuania = 0x1F,
        [EnumDescription(Text = "Luxembourg (L)")]
        Luxembourg = 0x1E,
        [EnumDescription(Text = "Macedonia (FYROM) (MK)")]
        Macedonia = 0x24,
        [EnumDescription(Text = "Malta (M)")]
        Malta = 0x21,
        [EnumDescription(Text = "Moldova (MD)")]
        Moldova = 0x23,
        [EnumDescription(Text = "Monaco (MC)")]
        Monaco = 0x22,
        //[EnumDescription(Text = "")]
        //Montenegro  Montenegro  MNE	0x34,   New nation not reported on (EC) 1360/2002 Appendix 1.
        [EnumDescription(Text = "Netherlands (NL)")]
        Netherlands = 0x26,
        [EnumDescription(Text = "Norway (N)")]
        Norway = 0x25,
        [EnumDescription(Text = "Poland (PL)")]
        Poland = 0x28,
        [EnumDescription(Text = "Portugal (P)")]
        Portugal = 0x27,
        [EnumDescription(Text = "Romania (RO)")]
        Romania = 0x29,
        [EnumDescription(Text = "Russia (RUS)")]
        Russia = 0x2B,
        [EnumDescription(Text = "San Marino (RSM)")]
        San_Marino = 0x2A,
        //[EnumDescription(Text = "")]
        //Serbia  Serbia  SRB	0x35,   New nation not reported on (EC) 1360/2002 Appendix 1.
        [EnumDescription(Text = "Slovakia (SK)")]
        Slovakia = 0x2D,
        [EnumDescription(Text = "Slovenia (SLO)")]
        Slovenia = 0x2E,
        [EnumDescription(Text = "Spain (E)")]
        Spain = 0x0F,
        [EnumDescription(Text = "Sweden (S)")]
        Sweden = 0x2C,
        [EnumDescription(Text = "Switzerland (CH)")]
        Switzerland = 0x0A,
        [EnumDescription(Text = "Tajikistan (TJ)")]
        Tajikistan = 0x37,
        [EnumDescription(Text = "Turkey (TR)")]
        Turkey = 0x30,
        [EnumDescription(Text = "Turkmenistan (TM)")]
        Turkmenistan = 0x2F,
        [EnumDescription(Text = "Ukraine (UA)")]
        Ukraine = 0x31,
        //[EnumDescription(Text = "")]
        //United Kingdom  United Kingdom  UK	0x15,   Assigned by ISO-3166 and UN Convention on Road Traffic.
        //    [EnumDescription(Text = "")]
        //Uzbekistan  Uzbekistan  UZ	0x36,   New nation not reported on 0xEC) 1360/2002 Appendix 1.
        [EnumDescription(Text = "Vatican City (V)")]
        Vatican_City = 0x32,
        //[EnumDescription(Text = "")]
        //Yugoslavia  YU	0x33,   Code no longer in use since 2003.
        //   [EnumDescription(Text = "Reserved for Future Use")]
        //RFU	0x38..FC,
        [EnumDescription(Text = "Community (EC)")]
        European = 0xFD,
        [EnumDescription(Text = "Rest of Europe (EUR)")]
        Rest_of_Europe = 0xFE,
        [EnumDescription(Text = "Rest of the World (WLD)")]
        Rest_of_the_World = 0xFF
    }
}
