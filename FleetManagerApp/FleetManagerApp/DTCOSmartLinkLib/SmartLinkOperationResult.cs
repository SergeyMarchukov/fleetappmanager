﻿using FleetManagerApp.DTCOSmartLinkLib.Enums;
using FleetManagerApp.Enums;
using FleetManagerApp.Extensions;
using Newtonsoft.Json.Linq;
using System;
using System.Text;

/// <summary>
/// Created by Sergey M. 20.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib
{
    public class SmartLinkOperationResult
    {
        private SmartLinkErrorCodeEnum _smartlinkErrorCode = SmartLinkErrorCodeEnum.NO_ERROR;
        private BluetoothConnectionErrorEnum _btErrorCode = BluetoothConnectionErrorEnum.NO_ERROR;
        private SmartLinkNegativeResponseEnum? _negativeResponse = null;
        private Object mData = new Object();

        public bool IsOk { get; private set; }
        public bool IsNegativeResponse { get; private set; } = false;
        //public byte SessionToken { get; private set; }
        public string Message { get; private set; } = "";

        //TODO Remove
        public SmartLinkMessage SmartLinkMessage { get; private set; }
        
        //public SmartLinkCmd cmd;

        public SmartLinkOperationResult Factory(SmartLinkMessage smartLinkMessage)
        {
            if (smartLinkMessage == null) return null;  // ???
            
            //TODO Remove
            SmartLinkMessage = smartLinkMessage;

            switch (smartLinkMessage.SmartLinkInterface)
            {
                case SmartLinkInterfaceEnum.KLINE:
                case SmartLinkInterfaceEnum.DOWNLOAD:
                case SmartLinkInterfaceEnum.CONFIGURATION:
                case SmartLinkInterfaceEnum.UPGRADE:
                case SmartLinkInterfaceEnum.SESSION_HANDLING:
                    SetData(smartLinkMessage);
                    break;
                case SmartLinkInterfaceEnum.ERROR:
                    SetError(smartLinkMessage);
                    break;
            }

            return this;
        }

        public SmartLinkOperationResult SetData(SmartLinkMessage smartLinkMessage)
        {
            IsOk = true;
            Message = smartLinkMessage.ToString();

            if(smartLinkMessage.Data[0] == 0x7E)
            {
                IsNegativeResponse = true;
                SetError((SmartLinkNegativeResponseEnum)smartLinkMessage.Data[2]);
            }

            return this;
        }

        public SmartLinkOperationResult SetError(SmartLinkMessage smartLinkMessage)
        {
            IsOk = false;
            _smartlinkErrorCode = (SmartLinkErrorCodeEnum)smartLinkMessage.Packet[6];

            Message = string.Format("ERROR: {0}", _smartlinkErrorCode.GetDescription());

            return this;
        }
        
        public SmartLinkOperationResult SetError(BluetoothConnectionErrorEnum code)
        {
            IsOk = false;
            _btErrorCode = code;

            Message = string.Format("ERROR: {0};", _btErrorCode.GetDescription());

            return this;
        }

        public SmartLinkOperationResult SetError(SmartLinkNegativeResponseEnum negativeResponse)
        {
            _negativeResponse = negativeResponse;
            Message = string.Format("NEGATIVE RESPONSE: {0}", negativeResponse.GetDescription());

            return this;
        }

        public SmartLinkOperationResult SetError(string message)
        {
            IsOk = false;
            Message = message;

            return this;
        }
        
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (_smartlinkErrorCode != SmartLinkErrorCodeEnum.NO_ERROR)
            { sb.Append(string.Format("SMARTLINK ERROR: {0}; MESSAGE: {1}", _smartlinkErrorCode.GetDescription(), Message)); }
            if(_btErrorCode != BluetoothConnectionErrorEnum.NO_ERROR)
            { sb.Append(string.Format("BLUETOOTH CONNECTION ERROR: {0};", _btErrorCode.GetDescription())); }
            
            if (mData is JObject)
            {
                sb.Append(String.Format("\nDATA: {0};", ((JObject)mData).ToString()));
            }

            return sb.ToString();
        }

        //////////////////////
        /*
        public static SmartLinkOperationResult FactoryError()
        {
            return new SmartLinkOperationResult().SetError(SmartLinkErrorCodeEnum.GENERAL_ERROR);
        }

        public static SmartLinkOperationResult FactoryError(String m)
        {
            return new SmartLinkOperationResult().SetError(m);
        }

        public static SmartLinkOperationResult FactoryError(String m, SmartLinkErrorCodeEnum c)
        {
            return new SmartLinkOperationResult().SetError(c, m);
        }

        public static SmartLinkOperationResult FactoryError(SmartLinkErrorCodeEnum c)
        {
            return new SmartLinkOperationResult().SetError(c);
        }

        public static SmartLinkOperationResult Factory()
        {
            return new SmartLinkOperationResult();
        }

        public static SmartLinkOperationResult Factory(Object d)
        {
            return new SmartLinkOperationResult().SetData(d);
        }

        public Object GetData()
        {
            return mData;
        }

        public SmartLinkOperationResult SetData(Object data)
        {
            if (data != null)
            {
                mData = data;
            }

            return this;
        }

        public SmartLinkOperationResult SetError(String m)
        {
            error_code = SmartLinkErrorCodeEnum.GENERAL_ERROR;
            Message = m;
            return this;
        }
        
        public SmartLinkOperationResult SetError(SmartLinkErrorCodeEnum code)
        {
            error_code = code;
            Message = code.GetDescription();
            return this;
        }

        */
    }
}
