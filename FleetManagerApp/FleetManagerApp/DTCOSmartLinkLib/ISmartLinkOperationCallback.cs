﻿/// <summary>
/// Created by Sergey M. 20.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib
{
    public interface ISmartLinkOperationCallback
    {
        void OnSmartLinkOperationResult();
    }
}
