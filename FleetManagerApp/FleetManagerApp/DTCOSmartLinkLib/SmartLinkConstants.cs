/// <summary>
/// Created by Sergey M. 10.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib
{
    public class SmartLinkConstants
    {
        public const string SESSION_TOKEN = "SessionToken";


        public static readonly int ERROR_RESPONSE_TIMEOUT = 0xFF;
        public static readonly int ERROR_RESPONSE_BUSY = 0xFE;
        public static readonly int ERROR_RESPONSE_NO_DATA = 0xFD;
        public static readonly int ERROR_RESPONSE_BUFFER_OVERFLOW = 0xFC;
        public static readonly int ERROR_RESPONSE_TRANSMISSION_ERROR = 0xFB;
        public static readonly int ERROR_RESPONSE_INVALID_TOKEN = 0xFA;
        public static readonly int ERROR_RESPONSE_TOKEN_ERROR = 0xF9;
        public static readonly int ERROR_RESPONSE_OVER_SESSION_ACTIVE = 0xF8;
        public static readonly int ERROR_RESPONSE_RESEND_BLOCK = 0xF7;
        public static readonly int ERROR_RESPONSE_WRONG_ID = 0xF6;
        public static readonly int ERROR_RESPONSE_NAK = 0xF5;
        public static readonly int ERROR_RESPONSE_SL_UPDATE_MODE = 0xF4;
        public static readonly int ERROR_RESPONSE_WRONG_PIN = 0xF3;
    }
}