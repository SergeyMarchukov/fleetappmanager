using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

/// <summary>
/// Created by Sergey M. 19.03.17
/// </summary>
namespace FleetManagerApp.DTCOSmartLinkLib
{
    public class SmartLinkPIN
    {
        public int IntValue { get; private set; }

        public List<byte> ByteValue { get; private set; }

        public bool Valid { get; private set; }

        public SmartLinkPIN(string pin)
        {
            ByteValue = new List<byte>();
            
            string pattern = @"\d+";
            Regex reg = new Regex(pattern);

            if (!string.IsNullOrEmpty(pin) && reg.IsMatch(pin))
            {
                Valid = true;

                IntValue = Convert.ToInt32(pin);

                ByteValue.Add(0x00);
                for (int i = 0; i < pin.Length; i++)
                {
                    ByteValue.Add(Convert.ToByte(pin[i]));
                }
            }
            else
            { 
                Valid = false;
            }
        }
    }
}