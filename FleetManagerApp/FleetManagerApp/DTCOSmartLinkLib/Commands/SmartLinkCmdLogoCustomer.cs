﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdLogoCustomer : SmartLinkCmdBase
    {
        public SmartLinkCmdLogoCustomer(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.LogoCustomer)
        { }
    }
}
