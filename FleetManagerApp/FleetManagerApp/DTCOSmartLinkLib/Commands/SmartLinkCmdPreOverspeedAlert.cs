﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdPreOverspeedAlert : SmartLinkCmdBase
    {
        public SmartLinkCmdPreOverspeedAlert(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.PreOverspeedAlert)
        { }
    }
}
