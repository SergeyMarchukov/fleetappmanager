using FleetManagerApp.DTCOSmartLinkLib.Enums;
using FleetManagerApp.Tool;
using System;
using System.Collections.Generic;

namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public abstract class SmartLinkCmdBase
    {
        protected SmartLinkInterfaceEnum SmartLinkInterface { get; set; } = SmartLinkInterfaceEnum.KLINE;
        protected byte RFU = 0xFF;
        protected byte _sessionToken;
        protected byte[] _cmdID;
        protected byte[] _cmdWriteData;

        public byte[] Message { get; protected set; }

        protected const byte DTCO_COMMAND_NEGATIVE_RESPONSE = 0x7F;
        protected const byte DTCO_READ_REQUEST_PREFIX = 0x22;
        protected const byte DTCO_READ_RESPONSE_PREFIX = 0x62;
        protected const byte DTCO_WRITE_REQUEST_PREFIX = 0x2E;
        protected const byte DTCO_WRITE_RESPONSE_PREFIX = 0x6E;

        //protected SmartLinkCmdBase()
        //{ }

        protected SmartLinkCmdBase(byte sessionToken)
        {
            SmartLinkInterface = SmartLinkInterfaceEnum.SESSION_HANDLING;
            _sessionToken = sessionToken;
        }

        protected SmartLinkCmdBase(byte sessionToken, SmartLinkCmdRDBIDictionaryEnum cmdID)
        {
            _sessionToken = sessionToken;
            _cmdID = Helpers.StringToByteArrayFastest(((int)cmdID).ToString("X2"));
        }

        protected byte GetChecksum(byte[] byteArray)
        {
            int cs = 0;

            for (int i = 0; i < byteArray.Length; i++)
            {
                cs = cs ^ byteArray[i];
            }

            int d = cs & 0xff;
            //return String.format("%02X", d);
            return Convert.ToByte(d);
        }

        protected byte[] GetTransformedLength(byte[] data)
        {
            byte[] length = new byte[2];
            length[0] = 0x00;
            length[1] = Convert.ToByte(data.Length);
            return length;
        }

        public byte[] GetReadMessage()
        {
            List<byte> mData = new List<byte>();
            mData.Add((byte)SmartLinkInterface);        // interface
            mData.Add(Convert.ToByte(_sessionToken));   // session token
            mData.Add(RFU);                             // RFU
            var dataWithPrefix = GetDataWithPrefix(DTCO_READ_REQUEST_PREFIX);
            mData.AddRange(GetTransformedLength(dataWithPrefix));   // length of data
            mData.AddRange(dataWithPrefix);             // add data
            mData.Add(GetChecksum(mData.ToArray()));    // checksum

            return mData.ToArray();
        }

        public byte[] GetWriteMessage()
        {
            List<byte> mData = new List<byte>();
            mData.Add((byte)SmartLinkInterface);        // interface
            mData.Add(Convert.ToByte(_sessionToken));   // session token
            mData.Add(RFU);                             // RFU
            var dataWithPrefix = GetDataWithPrefix(DTCO_WRITE_REQUEST_PREFIX);
            mData.AddRange(GetTransformedLength(dataWithPrefix));   // length of data
            mData.AddRange(dataWithPrefix);             // add data
            mData.Add(GetChecksum(mData.ToArray()));    // checksum

            return mData.ToArray();
        }

        private byte[] GetDataWithPrefix(byte prefix)
        {
            //byte[] newarr = new byte[_cmdID.Length + 1];
            //newarr[0] = prefix;

            //_cmdID.CopyTo(newarr, 1);

            //if (_cmdWriteData != null && _cmdWriteData.Length > 0)
            //{ _cmdWriteData.CopyTo(newarr, _cmdID.Length + 1); }

            List<byte> newarr = new List<byte>();
            newarr.Add(prefix);
            newarr.AddRange(_cmdID);

            if (_cmdWriteData != null && _cmdWriteData.Length > 0)
                newarr.AddRange(_cmdWriteData);

            return newarr.ToArray();
        }

        public object ParseCommandResponse(SmartLinkMessage msg)
        {

            return null;
        }
    }
}