﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdISOREplusCfg : SmartLinkCmdBase
    {
        public SmartLinkCmdISOREplusCfg(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.ISOREplusCfg)
        { }
    }
}
