﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdOptionalFeaturesCfg : SmartLinkCmdBase
    {
        public SmartLinkCmdOptionalFeaturesCfg(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.OptionalFeaturesCfg)
        { }
    }
}
