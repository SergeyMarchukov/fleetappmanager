﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdCompanyLocalTime : SmartLinkCmdBase
    {
        public SmartLinkCmdCompanyLocalTime(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.CompanyLocalTime)
        { }
    }
}
