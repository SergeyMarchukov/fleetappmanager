﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdRegisteringMemberState : SmartLinkCmdBase
    {
        public SmartLinkCmdRegisteringMemberState(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.RegisteringMemberState)
        { }

        public SmartLinkCmdRegisteringMemberState(byte sessionToken, byte[] writeData) 
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.RegisteringMemberState)
        {
            if (writeData != null && writeData.Length > 0)
            {
                _cmdWriteData = writeData;
            }
        }
    }
}
