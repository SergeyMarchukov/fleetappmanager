﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdEnterVDOlicence : SmartLinkCmdBase
    {
        public SmartLinkCmdEnterVDOlicence(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.EnterVDOlicence)
        { }
    }
}
