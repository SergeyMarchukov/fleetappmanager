﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdTimeBetweenTwoMandatoryDriverCardDownloads : SmartLinkCmdBase
    {
        public SmartLinkCmdTimeBetweenTwoMandatoryDriverCardDownloads(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.TimeBetweenTwoMandatoryDriverCardDownloads)
        { }
    }
}
