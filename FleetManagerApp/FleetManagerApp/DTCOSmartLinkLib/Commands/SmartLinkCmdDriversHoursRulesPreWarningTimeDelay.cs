﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdDriversHoursRulesPreWarningTimeDelay : SmartLinkCmdBase
    {
        public SmartLinkCmdDriversHoursRulesPreWarningTimeDelay(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.DriversHoursRulesPreWarningTimeDelay)
        { }
    }
}
