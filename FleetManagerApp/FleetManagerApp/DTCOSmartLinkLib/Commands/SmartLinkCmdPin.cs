using FleetManagerApp.DTCOSmartLinkLib.Enums;
using System;
using System.Collections.Generic;

namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdPin : SmartLinkCmdBase
    {        
        public SmartLinkCmdPin(byte sessionToken, byte[] pin) : base(sessionToken)
        {
            SmartLinkInterface = SmartLinkInterfaceEnum.SESSION_HANDLING;

            if (pin.Length > 0)
            {
                List<byte> mData = new List<byte>();
                mData.Add((byte)SmartLinkInterface);        // interface
                mData.Add(Convert.ToByte(sessionToken));    // session token
                mData.Add(RFU);                             // RFU (FF)
                //mData.Add(0x00);                            // data length low byte
                //mData.Add(Convert.ToByte((data.Length)));   // data length high byte
                mData.AddRange(GetTransformedLength(pin));
                mData.AddRange(pin);                       // data
                mData.Add(GetChecksum(mData.ToArray()));    // checksum

                Message = mData.ToArray();
            }
        }        
    }
}