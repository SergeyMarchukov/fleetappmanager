﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdRemoteDownloadCalibrationInterfaceCfg : SmartLinkCmdBase
    {
        public SmartLinkCmdRemoteDownloadCalibrationInterfaceCfg(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.RemoteDownloadCalibrationInterfaceCfg)
        { }
    }
}
