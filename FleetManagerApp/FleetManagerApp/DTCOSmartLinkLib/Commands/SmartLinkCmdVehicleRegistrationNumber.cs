﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdVehicleRegistrationNumber : SmartLinkCmdBase
    {
        public SmartLinkCmdVehicleRegistrationNumber(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.VehicleRegistrationNumber)
        { }
    }
}
