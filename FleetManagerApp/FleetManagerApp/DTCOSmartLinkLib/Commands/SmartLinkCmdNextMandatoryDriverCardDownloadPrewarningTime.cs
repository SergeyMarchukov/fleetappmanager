﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdNextMandatoryDriverCardDownloadPrewarningTime : SmartLinkCmdBase
    {
        public SmartLinkCmdNextMandatoryDriverCardDownloadPrewarningTime(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.NextMandatoryDriverCardDownloadPrewarningTime)
        { }
    }
}
