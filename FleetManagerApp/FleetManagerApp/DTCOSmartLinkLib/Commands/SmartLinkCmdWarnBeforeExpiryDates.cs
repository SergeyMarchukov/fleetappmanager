﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdWarnBeforeExpiryDates : SmartLinkCmdBase
    {
        public SmartLinkCmdWarnBeforeExpiryDates(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.WarnBeforeExpiryDates)
        { }
    }
}
