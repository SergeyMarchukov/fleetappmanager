﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdVehicleCANmessagesCfg : SmartLinkCmdBase
    {
        public SmartLinkCmdVehicleCANmessagesCfg(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.VehicleCANmessagesCfg)
        { }
    }
}
