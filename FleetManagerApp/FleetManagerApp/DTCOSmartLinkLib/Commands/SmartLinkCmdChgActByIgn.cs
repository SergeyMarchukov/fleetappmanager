﻿namespace FleetManagerApp.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdChgActByIgn : SmartLinkCmdBase
    {
        public SmartLinkCmdChgActByIgn(byte sessionToken)
            : base(sessionToken, Enums.SmartLinkCmdRDBIDictionaryEnum.ChgActByIgn)
        { }
    }
}
