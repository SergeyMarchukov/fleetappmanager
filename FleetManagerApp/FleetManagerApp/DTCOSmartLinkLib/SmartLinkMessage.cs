using FleetManagerApp.DTCOSmartLinkLib.Enums;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace FleetManagerApp.DTCOSmartLinkLib
{
    public class SmartLinkMessage
    {

        public SmartLinkInterfaceEnum SmartLinkInterface { get; private set; }

        private byte _sessionToken;
        public byte SessionToken
        {
            get { return _sessionToken; }
            set
            {
                _sessionToken = value;
                UpdateChecksum();
            }
        }

        public List<byte> Packet { get; private set; }
        public byte[] Data { get; private set; }
        public int DataLength { get; private set; }
        public string StringData { get; private set; }
        public int CheckSum { get; private set; }

        // TODO
        public SmartLinkMessage(byte[] message)
        {
            Packet = new List<byte>();

            SmartLinkInterface = (SmartLinkInterfaceEnum)Enum.Parse(typeof(SmartLinkInterfaceEnum), message[0].ToString());
            Packet.Add(message[0]);

            _sessionToken = message[1];
            Packet.Add(message[1]);

            Packet.Add(message[2]);
            
            DataLength = message[3] + message[4];
            Packet.Add(message[3]);
            Packet.Add(message[4]);

            int i;

            StringBuilder sb = new StringBuilder();
            

            Data = new byte[DataLength];
            //if (DataLength > 3)
            //{
            //    Data = new byte[DataLength - 3];
            //}

            for (i = 0; i < DataLength; i++)
            {
                Packet.Add(message[i + 5]);
                sb.Append(string.Format("{0:X2}", message[i + 5]));
                if (i > 2)
                {
                    Data[i - 3] = message[i + 5];
                }
            }
            Packet.Add(message[i + 5]);

            StringData = sb.ToString();
            int src = message[5 + i];
            CheckSum = src < 0 ? src + 256 : src;
        }

        public override string ToString()
        {
            return string.Format("Interface: {0}, SessionToken: {1:X2}, DataLength: {2}, Data: {3}, CheckSum: {4:D}",
                SmartLinkInterface, _sessionToken, DataLength, StringData, CheckSum);

            //return "Interface: " + SmartLinkInterface + ", SessionToken: " + string.Format("{0:X2}", _sessionToken)
            //        + ", DataLength: " + DataLength + ", Data: " + StringData + ", Checksum: "
            //        + string.Format("{0:D}", CheckSum);
            
        }

        public JObject ToJSON()// throws JSONException
        {
            JObject json = new JObject();
            json.Add("iFace", (int)SmartLinkInterface);
            json.Add("sessionToken", _sessionToken);
            json.Add("length", DataLength);
            json.Add("data", StringData);
            json.Add("checksum", string.Format("{0:D}", CheckSum));
            return json;
        }

        /////////////////////////////////////////////

        // TODO
        private void UpdateChecksum()
        {
            //string s = string.Format("%02X", SmartLinkInterface) + string.Format("%02X", _sessionToken) + "FF"
            //        + string.Format("%04X", DataLength) + BitConverter.ToString(Data);

            //int len = s.Length;
            //byte[] data = new byte[len / 2];
            //for (int i = 0; i < len; i += 2)
            //{
            //    data[i / 2] = (byte)((Java.Lang.Character.Digit(s[i], 16) << 4) + Java.Lang.Character.Digit(s[i + 1], 16));
            //}

            //int cs = 0;
            //for (int i = 0; i < data.Length; i++)
            //{
            //    cs = cs ^ data[i];
            //}

            //CheckSum = cs & 0xff;
        }

        #region Implemented methods

        //private int _sessionToken;
        //public void setSessionToken(int sessionToken)
        //{
        //    mSessionToken = sessionToken;
        //    UpdateChecksum();
        //}

        //public int getSessionToken()
        //{
        //    return mSessionToken;
        //}

        //private int mIface;
        //public int getIface()
        //{
        //    return mIface;
        //}

        //private int mDataLength;
        //public int getDataLength()
        //{
        //    return mDataLength;
        //}

        //private string mData;
        //public string getData()
        //{
        //    return mData;
        //}

        //private int mChecksum;
        //public int getChecksum()
        //{
        //    return mChecksum;
        //}

        //private byte[] mRawMessage = new byte[0];
        //public byte[] getRawMessage()
        //{
        //    return mRawMessage;
        //}

        #endregion


        // TODO
        public string getHexCheckSum()
        {
            string strCS = string.Format("%02X", CheckSum);
            if (strCS.Length > 2)
            {
                return strCS.Substring(strCS.Length - 2, strCS.Length);
            }
            else
            {
                return strCS;
            }
        }

        // TODO
        public byte[] getByteArray()
        {
            //string s = toHexString();

            //int len = s.Length;
            //byte[] data = new byte[len / 2];
            //for (int i = 0; i < len; i += 2)
            //{
            //    data[i / 2] = (byte)((Java.Lang.Character.Digit(s[i], 16) << 4) + Java.Lang.Character.Digit(s[i + 1], 16));
            //}
            //return data;
            return null;
        }

        // TODO
        //public string toHexString()
        //{
        //    return string.Format("%02X", SmartLinkInterface) + string.Format("%02X", _sessionToken) + "FF"
        //            + string.Format("%04X", DataLength) + BitConverter.ToString(Data) + string.Format("%02X", CheckSum);
        //}
        
    }
}