﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LicenseAgreement : ContentPage
    {
        public LicenseAgreement()
        {
            InitializeComponent();

            Init();
        }

        public void Init()
        {
            Title = "License Agreement";

            btnAcceptLicense.Clicked += BtnAcceptLicense_Clicked;
        }

        private async void BtnAcceptLicense_Clicked(object sender, EventArgs e)
        {
            var accepted = await DisplayAlert("License", "License was accepted!", "Ok", "Cancel");
            if (accepted)
            {
                Navigation.PopAsync(true);
            }
        }
    }
}
