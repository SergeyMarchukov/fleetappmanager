﻿using FleetManagerApp.BTInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FleetManagerApp
{
    public partial class MainPage : ContentPage
    {
        IBTManager BTManager;

        public MainPage()
        {
            InitializeComponent();

            Init();
        }

        public void Init()
        {
            BTManager = BTManagerSingleton.BTManagerInstance;
            if (!BTManager.IsEnable)
            { BTManager.IntentEnableBluetooth(); }

            TapGestureRecognizer recognizer = new TapGestureRecognizer();
            recognizer.Tapped += Recognizer_Tapped;
            this.Content.GestureRecognizers.Add(recognizer);            
        }

        private async void Recognizer_Tapped(object sender, EventArgs e)
        {
            App.Current.MainPage = new NavigationPage(new ConnectPage());
        }
    }
}
