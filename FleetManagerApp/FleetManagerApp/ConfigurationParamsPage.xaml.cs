﻿using FleetManagerApp.BTInterfaces;
using FleetManagerApp.DTCOSmartLinkLib;
using FleetManagerApp.DTCOSmartLinkLib.Commands;
using FleetManagerApp.Models;
using FleetManagerApp.Tool;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FleetManagerApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfigurationParamsPage : ContentPage
    {
        private IBTManager _btManager;

        public ConfigurationParamsPage()
        {
            InitializeComponent();

            Init();
        }

        public void Init()
        {
            Title = "Configuration Page";

            _btManager = DependencyService.Get<IBTManager>();

            listConfigurations.ItemTapped += ListConfigurations_ItemTapped;
            listConfigurations.ItemsSource = GetList();
        }

        private async void ListConfigurations_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            listConfigurations.SelectedItem = null;

            if (!_btManager.IsEnable)
            {
                _btManager.IntentEnableBluetooth();
                return;
            }

            if (!AppDataStorage.Exists(SmartLinkConstants.SESSION_TOKEN) || !_btManager.IsConnected)
            {
                DisplayAlert("", "Connection was not established!", "Ok");
                return;
            }

            var selectedParam = e.Item as ParameterConfigurationModel;
            if (selectedParam != null)
            {
                if (selectedParam.TypeOfPage != null)
                {
                    AppDataStorage.Set("cmdType", selectedParam.TypeOfCmd);

                    Page page = (Page)Activator.CreateInstance(selectedParam.TypeOfPage);
                    
                    await Navigation.PushAsync(page);
                }
                else
                { DisplayAlert("", "Item does not have Type of Page", "Ok"); }
            }
        }

        private List<ParameterConfigurationGroupModel> GetList()
        {
            var FeaturesList = new ParameterConfigurationGroupModel()
            {
                new ParameterConfigurationModel(){ Title = "VehicleCANmessagesCfg", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdVehicleCANmessagesCfg)},
                new ParameterConfigurationModel(){ Title = "ISOREplusCfg", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdISOREplusCfg)},
                new ParameterConfigurationModel(){ Title = "ChgActByIgn", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdChgActByIgn)},
                new ParameterConfigurationModel(){ Title = "CompanyLocalTime", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdCompanyLocalTime)}
            };
            FeaturesList.Title = "Feature Configuration";

            var WarningsList = new ParameterConfigurationGroupModel()
            {
                new ParameterConfigurationModel(){ Title = "DriversHoursRulesPreWarningTimeDelay", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdDriversHoursRulesPreWarningTimeDelay)},
                new ParameterConfigurationModel(){ Title = "PreOverspeedAlert", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdPreOverspeedAlert)},
                new ParameterConfigurationModel(){ Title = "NextMandatoryDriverCardDownloadPrewarningTime", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdNextMandatoryDriverCardDownloadPrewarningTime)},
                new ParameterConfigurationModel(){ Title = "TimeBetweenTwoMandatoryDriverCardDownloads", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdTimeBetweenTwoMandatoryDriverCardDownloads)},
                new ParameterConfigurationModel(){ Title = "WarnBeforeExpiryDates", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdWarnBeforeExpiryDates)}
            };
            WarningsList.Title = "Warnings / Reminder";

            var VRNList = new ParameterConfigurationGroupModel()
            {
                new ParameterConfigurationModel(){ Title = "RegisteringMemberState", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(MemberStatePage), TypeOfCmd = typeof(SmartLinkCmdRegisteringMemberState)},
                new ParameterConfigurationModel(){ Title = "VehicleRegistrationNumber", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdVehicleRegistrationNumber)}
            };
            VRNList.Title = "Vehicle Registration Number";

            var LicFeaturesList = new ParameterConfigurationGroupModel()
            {
                new ParameterConfigurationModel(){ Title = "EnterVDOlicence", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdEnterVDOlicence)},
                new ParameterConfigurationModel(){ Title = "RemoteDownloadCalibrationInterfaceCfg", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdRemoteDownloadCalibrationInterfaceCfg)},
                new ParameterConfigurationModel(){ Title = "OptionalFeaturesCfg", Permissions = Enums.ParamPermissionsEnum.ReadWrite, TypeOfPage = typeof(GeneralCommandPage), TypeOfCmd = typeof(SmartLinkCmdOptionalFeaturesCfg)}
            };
            LicFeaturesList.Title = "Licensed Feature";

            return new List<ParameterConfigurationGroupModel>() { FeaturesList, WarningsList, VRNList, LicFeaturesList };
        }

    }
}
