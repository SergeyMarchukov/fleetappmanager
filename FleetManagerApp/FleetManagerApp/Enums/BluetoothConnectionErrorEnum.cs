﻿using FleetManagerApp.Attributes;

/// <summary>
/// Created by Sergey M. 24.03.17
/// </summary>
namespace FleetManagerApp.Enums
{
    public enum BluetoothConnectionErrorEnum
    {
        [EnumDescription(Text = "NO ERROR")]
        NO_ERROR,
        [EnumDescription(Text = "GENERAL ERROR")]
        GENERAL_ERROR,
        [EnumDescription(Text = "BLUETOOTH NOT ENABLE")]
        BLUETOOTH_NOT_ENABLE,
        [EnumDescription(Text = "BLUETOOTH NOT CONNECTED")]
        BLUETOOTH_NOT_CONNECTED,
        [EnumDescription(Text = "SESSION NOT OPEN")]
        SESSION_NOT_OPEN,
        [EnumDescription(Text = "BLUETOOTH DEVICE NOT FOUND")]
        BLUETOOTH_DEVICE_NOT_FOUND,
        [EnumDescription(Text = "BLUETOOTH DEVICE NOT SMARTLINK")]
        BLUETOOTH_DEVICE_NOT_SMARTLINK,
        [EnumDescription(Text = "BLUETOOTH CONNECTION LOST")]
        BLUETOOTH_CONNECTION_LOST,
        [EnumDescription(Text = "BLUETOOTH CONNECTION FAILED")]
        BLUETOOTH_CONNECTION_FAILED,
        [EnumDescription(Text = "COMMAND REPLY WRONG CHECKSUM")]
        COMMAND_REPLY_WRONG_CHECKSUM
    }
}
