﻿namespace FleetManagerApp.Enums
{
    public enum ParamPermissionsEnum
    {
        Read,
        Write,
        ReadWrite,
        ReadLicense
    }
}
