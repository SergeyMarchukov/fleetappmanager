﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using FleetManagerApp.BTInterfaces;
using CoreBluetooth;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(FleetManagerApp.iOS.BTClasses.BTManager))]
namespace FleetManagerApp.iOS.BTClasses
{
    public class BTManager : IBTManager
    {
        private readonly string _uuid = "00001101-0000-1000-8000-00805f9b34fb";

        private CBCentralManager _nativeManager;

        public bool IsEnable { get { return _nativeManager.State == CBCentralManagerState.PoweredOn; } }

        public BTManager()
        {
            _nativeManager = new CBCentralManager();
            _nativeManager.UpdatedState += OnUpdatedState;
        }

        private void OnUpdatedState(object sender, EventArgs e)
        {
            switch (_nativeManager.State)
            {   
                case CBCentralManagerState.PoweredOn:
                    break;
                case CBCentralManagerState.PoweredOff:
                    break;
                case CBCentralManagerState.Unauthorized:
                    break;
                case CBCentralManagerState.Unsupported:
                    break;
                case CBCentralManagerState.Resetting:
                    break;
                case CBCentralManagerState.Unknown:
                    break;
                default:
                    break;
            }
        }

        public ObservableCollection<IBTDevice> GetPairedDevices()
        {
            throw new NotImplementedException();
        }

        public void IntentEnableBluetooth()
        {
            if (_nativeManager.State == CBCentralManagerState.PoweredOff)
            {
                UIAlertView _error = new UIAlertView("Info", "Please turn on bluetooth", null, "Ok", null);

                _error.Show();
            }
            //throw new NotImplementedException();
        }

        /// <summary>
        /// TODO: Implement
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IBTManager other)
        {
            return true;
        }
    }
}
