﻿using FleetManagerApp.Tool;
using System.Diagnostics;

[assembly: Xamarin.Forms.Dependency(typeof(FleetManagerApp.iOS.Tool.Logger))]
namespace FleetManagerApp.iOS.Tool
{
    public class Logger : ILogger
    {
        private readonly string Tag = "DEVELOPMENT";
        private readonly string ErrorMarker = "[########]";
        private readonly string InfoMarker = "[++++++++]";

        public void Error(string msg)
        {
            Debug.WriteLine("{0} {1}", ErrorMarker, msg);
        }

        public void Info(string msg)
        {
            Debug.WriteLine("{0} {1}", InfoMarker, msg);
        }
    }
}
