using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Util;
using FleetManagerApp.BTInterfaces;
using FleetManagerApp.DTCOSmartLinkLib;
using FleetManagerApp.Enums;
using Java.Util;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(FleetManagerApp.Droid.BTClasses.BTManager))]
namespace FleetManagerApp.Droid.BTClasses
{
    public class BTManager : IBTManager
    {
        private readonly string _uuid = "00001101-0000-1000-8000-00805F9B34FB";
        private readonly string Tag = "DEVELOPMENT";

        private BluetoothAdapter _btAdapter;
        private BluetoothDevice _currentDevice;
        private BluetoothSocket _btSocket = null;
        private Stream _inputStream = null;
        private Stream _outputStream = null;

        private byte[] _buffer;

        public bool IsDiscovering { get; private set; }

        public bool IsEnable { get { return BluetoothAdapter.DefaultAdapter.IsEnabled; } }

        public bool IsConnected { get { return _btSocket != null && _btSocket.IsConnected; } }

        public BTManager()
        {
            _btAdapter = BluetoothAdapter.DefaultAdapter;
        }

        public ObservableCollection<IBTDevice> GetPairedDevices()
        {
            ObservableCollection<IBTDevice> list = new ObservableCollection<IBTDevice>();

            foreach (var d in _btAdapter.BondedDevices.Where(x => x.Name.ToLower().Contains("smartlink")))
            {
                list.Add(new BTDevice(d));
            }

            return list;
        }

        public void IntentEnableBluetooth()
        {
            Intent intentEnableBt = new Intent(BluetoothAdapter.ActionRequestEnable);
            intentEnableBt.AddFlags(ActivityFlags.NewTask);
            Application.Context.StartActivity(intentEnableBt);
        }

        /// <summary>
        /// TODO: Implement
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IBTManager other)
        {
            return true;
        }

        /// <summary>
        /// Create RFCOMM Socket
        /// </summary>
        /// <param name="device"></param>
        public void CreateSocket(IBTDevice device)
        {
            _currentDevice = device.NativeBTDevice as BluetoothDevice;
            if (_currentDevice != null)
            {
                try
                {
                    if (_btSocket != null && _btSocket.IsConnected)
                    { _btSocket.Close(); }

                    _btSocket = _currentDevice.CreateRfcommSocketToServiceRecord(UUID.FromString(_uuid));
                }
                catch (Exception ex)
                {
                    Log.Error(Tag, "[########] ANDROID CREATING SOCKETERROR: {0}", ex.Message);
                }
            }
        }

        public async Task ConnectToRemoteDevice()
        {
            Log.Info(Tag, "[++++++++] TRY TO CONNECT TO REMOTE DEVICE");

            // Cancel discovery because it otherwise slows down the connection.
            if (_btAdapter.IsDiscovering)
            {
                _btAdapter.CancelDiscovery();
            }

            if (_btSocket != null)
            {

                try
                {
                    await _btSocket.ConnectAsync();
                }
                catch (Exception ex)
                {
                    _btSocket.Close();

                    Log.Error(Tag, "[########] ANDROID CONNECT ERROR: {0}", ex.Message);
                }
            }
        }

        public async Task<SmartLinkOperationResult> WriteAsync(byte[] buffer)
        {
            if (!_btSocket.IsConnected)
            {
                return new SmartLinkOperationResult().SetError(BluetoothConnectionErrorEnum.BLUETOOTH_NOT_CONNECTED);
            }

            await _btSocket.OutputStream.FlushAsync();
            await _btSocket.InputStream.FlushAsync();

            using (Stream output = _btSocket.OutputStream)
            {
                try
                {
                    while (true)
                    {
                        if(!output.CanWrite)
                        { continue; }

                        await output.WriteAsync(buffer, 0, buffer.Length);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(Tag, "[########] " + ex.Message);
                    return new SmartLinkOperationResult().SetError(ex.Message);
                }
            }

            return null;
        }

        public async Task<SmartLinkOperationResult> ReadAsync()
        {
            if (!_btSocket.IsConnected)
            {
                return new SmartLinkOperationResult().SetError(BluetoothConnectionErrorEnum.BLUETOOTH_NOT_CONNECTED);
            }

            //_buffer = new byte[256];
            using (Stream input = _btSocket.InputStream)
            {
                try
                {
                    while (true)
                    {
                        if (!input.CanRead || !input.IsDataAvailable())
                        { continue; }

                        //byte[] currBuffer = new byte[1024];
                        //int numBytes = await input.ReadAsync(currBuffer, 0, currBuffer.Length);
                        //_buffer = new byte[numBytes];
                        //Array.Copy(currBuffer, _buffer, numBytes);

                        _buffer = new byte[256];
                        int numBytes = await input.ReadAsync(_buffer, 0, _buffer.Length);

                        SmartLinkMessage smartLinkMessage = new SmartLinkMessage(_buffer);

                        Log.Info(Tag, "[++++++++] RECEIVED BYTES: [ " + BitConverter.ToString(_buffer) + " ]");

                        return new SmartLinkOperationResult().Factory(smartLinkMessage);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(Tag, "[########] " + ex.Message);
                    return new SmartLinkOperationResult().SetError(ex.Message);
                }
            }
        }
    }
}
