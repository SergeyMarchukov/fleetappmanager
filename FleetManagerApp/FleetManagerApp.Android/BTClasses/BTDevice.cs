using System;
using Android.Bluetooth;
using FleetManagerApp.BTInterfaces;

namespace FleetManagerApp.Droid.BTClasses
{
    public class BTDevice : IBTDevice
    {
        private BluetoothDevice _nativeBTDevice;

        public string Name { get { return _nativeBTDevice.Name; } }

        public string Address { get { return _nativeBTDevice.Address; } }

        public object NativeBTDevice { get { return _nativeBTDevice; } }
                

        public BTDevice(BluetoothDevice device)
        {
            _nativeBTDevice = device;
        }

        /// <summary>
        /// TODO Implement
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IBTDevice other)
        {
            return true;
        }
    }
}