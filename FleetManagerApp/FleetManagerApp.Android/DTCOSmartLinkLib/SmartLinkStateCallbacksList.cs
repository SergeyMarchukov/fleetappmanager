using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 10.03.17
    /// </summary>
    public class SmartLinkStateCallbacksList : List<ISmartLinkStateCallback>, ISmartLinkStateCallback
    {
        public void onSmartLinkDisconnected(SmartLinkOperationResult result)
        {
            foreach (ISmartLinkStateCallback item in this)
            {
                item.onSmartLinkDisconnected(result);
            }
        }

        public void onSmartLinkConnected(SmartLinkOperationResult result)
        {
            foreach (ISmartLinkStateCallback item in this)
            {
                item.onSmartLinkDisconnected(result);
            }
        }

        public void onSmartLinkUnProcessedResponse(SmartLinkOperationResult result)
        {
            foreach (ISmartLinkStateCallback item in this)
            {
                item.onSmartLinkUnProcessedResponse(result);
            }
        }
    }
}