using FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class SmartLinkCmdTimeDate : SmartLinkCmdRDBI
    {
        public TimeSeparated timeDate;


        public SmartLinkCmdTimeDate(ISmartLinkOperationCallback callback) : base(callback)
        {
            _initValuesFromDictionary(SmartLinkCmdRDBIDictionary.TimeDate);
        }


        //@Override
        public Object parseCommandResponse(SmartLinkMessage msg)
        {
            timeDate = new TimeSeparated(msg.getRawMessage());
            JObject json = new JObject();
            try
            {
                json.Add("timeDate", timeDate.toJSON());
            }
            catch (JsonException e)
            {
                //
            }
            return json;
        }
    }
}