using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Reflection;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 13.03.17
    /// </summary>
    public abstract class SmartLinkCmdDataType
    {
        public JObject toJSON()
        {

            JObject json = new JObject();

            foreach (FieldInfo f in this.GetType().GetFields())
            {
                try
                {
                    if (f.Name.Equals("_instance"))
                    {
                        continue;
                    }
                    try
                    {
                        json.Add(f.Name, JsonConvert.SerializeObject(f.GetValue(this).GetType().GetMethod("toJSON").Invoke(f.GetValue(this), new object[0])));
                        continue;
                    }
                    //catch (NoSuchMethodException e)
                    //{

                    //}
                    //catch (InvocationTargetException e)
                    //{

                    //}
                    catch (Exception E)
                    { }

                    json.Add(f.Name, JsonConvert.SerializeObject(f.GetValue(this)));

                }
                catch (JsonException e)
                {
                    // e.printStackTrace();
                }
                //catch (IllegalAccessException e)
                //{
                //    // e.printStackTrace();
                //}
                catch(Exception e)
                { }
            }
            return json;
        }

        public Object get(String name)
        {
            try
            {
                FieldInfo f = this.GetType().GetField(name);
                return f.GetValue(this);
            }
            //catch (NoSuchFieldException e)
            //{
            //    //e.printStackTrace();
            //}
            //catch (IllegalAccessException e)
            //{
            //    //e.printStackTrace();
            //}
            catch(Exception e)
            { }
            return null;
        }


        public static bool bitIsSet(byte b, int bitNum)
        {
            if (bitNum < 0 && bitNum > 7)
            {
                return false;
            }
            return (b & (0x01 << bitNum)) > 0;
        }

        public static IS_Info? getIS_Info(int i)
        {
            foreach (IS_Info isi in Enum.GetValues(typeof(IS_Info)))
            {
                if ((int)isi == i)
                {
                    return  isi;
                }
            }
            return null;
        }
        public static IS_Info getStatus(bool b1, bool b2)
        {
            if (b1 && b2)
            {
                return IS_Info.NOT_AVAILABLE;
            }
            else if (b1 && !b2)
            {
                return IS_Info.ERROR;
            }
            else if (!b1 && b2)
            {
                return IS_Info.VALUE1;
            }

            return IS_Info.VALUE0;
        }

        public static IS_Info getStatus(byte b, int pos1, int pos2)
        {
            return getStatus(bitIsSet(b, pos1), bitIsSet(b, pos2));
        }
    }
}