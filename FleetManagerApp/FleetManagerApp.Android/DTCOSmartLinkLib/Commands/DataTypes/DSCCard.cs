namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class DSCCard : SmartLinkCmdDataType
    {
        public bool cardInsertionIsDetectedInSlot2 = false;
        public bool cardInsertionIsDetectedInSlot1 = false;
        public bool cardInSlot1 = false;
        public bool cardInSlot2 = false;
        public bool cardInSlot1Locked = false;
        public bool cardInSlot2Locked = false;

        public DSCCard(byte o)
        {
            cardInsertionIsDetectedInSlot2 = bitIsSet(o, 0);
            cardInsertionIsDetectedInSlot1 = bitIsSet(o, 1);
            cardInSlot2 = bitIsSet(o, 2);
            cardInSlot1 = bitIsSet(o, 3);
            cardInSlot2Locked = bitIsSet(o, 4);
            cardInSlot1Locked = bitIsSet(o, 5);

        }
    }
}