namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class DSCVoltage : SmartLinkCmdDataType
    {
        public bool powerSupplyInterruptionIsActive = false;
        public bool significantVoltageDifference = false;
        public bool printerSupplyVoltageIsOff = false;
        public bool terminalA2voltageOff = false;
        public bool terminalA3voltageOff = false;
        public bool terminalA1voltageOff = false;

        public DSCVoltage(byte o)
        {
            powerSupplyInterruptionIsActive = bitIsSet(o, 2);
            significantVoltageDifference = bitIsSet(o, 3);
            printerSupplyVoltageIsOff = bitIsSet(o, 4);
            terminalA2voltageOff = bitIsSet(o, 5);
            terminalA3voltageOff = bitIsSet(o, 6);
            terminalA1voltageOff = bitIsSet(o, 7);
        }
    }
}