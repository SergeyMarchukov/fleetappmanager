using System;
using System.Text;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Create by Sergey M. 14.03.17
    /// </summary>
    public class CardNumber : SmartLinkCmdDataType
    {
        public string driverIdentification;
        public string ownerIdentification;

        public int cardReplacementIndex;
        public int cardRenewalIndex;

        public CardNumber(FullCardNumber.EquipmentType? type, byte[] b)
        {
            driverIdentification = "";
            ownerIdentification = "";
            if (type == FullCardNumber.EquipmentType.DriverCard)
            {
                byte[] bb = new byte[14];
                for (int i = 0; i < 14; i++)
                {
                    bb[i] = b[i];
                }
                cardReplacementIndex = SmartLinkHelpers.byte2uint32(b[14]);
                cardRenewalIndex = SmartLinkHelpers.byte2uint32(b[15]);

                driverIdentification = BitConverter.ToString(bb);   //Encoding.UTF8.GetString(bb); //new String(bb);
            }
            else if (type == FullCardNumber.EquipmentType.WorkshopCard
                   || type == FullCardNumber.EquipmentType.CompanyCard
                   || type == FullCardNumber.EquipmentType.ControlCard)
            {

                byte[] bb = new byte[13];
                for (int i = 0; i < 13; i++)
                {
                    bb[i] = b[i];
                }
                cardReplacementIndex = SmartLinkHelpers.byte2uint32(b[14]);
                cardRenewalIndex = SmartLinkHelpers.byte2uint32(b[15]);

                ownerIdentification = BitConverter.ToString(bb);    //Encoding.UTF8.GetString(bb);      //new String(bb);
            }
        }
    }
}