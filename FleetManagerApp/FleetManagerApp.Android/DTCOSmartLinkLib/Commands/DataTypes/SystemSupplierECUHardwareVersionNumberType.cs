using System;
using System.Text;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class SystemSupplierECUHardwareVersionNumberType : SmartLinkCmdDataType
    {
        public string prefixSystemLP;
        public string serialnumberSystemLP;
        public string separator;
        public string articleCodeSystemPCB;
        public string tail;

        public SystemSupplierECUHardwareVersionNumberType(byte[] b)
        {
            prefixSystemLP = "";
            serialnumberSystemLP = "";
            separator = "";
            articleCodeSystemPCB = "";
            tail = "";
            if (b.Length > 22)
            {
                string t = Encoding.UTF8.GetString(b); // Consider: BitConverter.ToString(b)         //new String(b);
                prefixSystemLP = t.Substring(0, 7);
                serialnumberSystemLP = t.Substring(7, 17);
                separator = t.Substring(17, 18);
                articleCodeSystemPCB = t.Substring(18, 22);
                tail = t.Substring(22);
            }
        }
    }
}