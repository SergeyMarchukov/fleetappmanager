namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /*
     * 
     *  Value assignment - Octet Aligned: 'xxxx xxxx xxxx xxxx xxxx ffee ddcc bbaa'
     *
     * 'xxxx xxxx xxxx xxxx xxxx'B: rfu, don't care. The DTCO accepts any value but overwrites it to 'not
     * available/not installed', i.e. sets all bits pairs to '11'B.
     * 'aa'H:
     *  '00'B the content of the TCO3and4 diagnostic data record is not available.
     *  '01'B the content of the TCO3and4 diagnostic data record is available.
     *  '10'B Error indicator
     * 'bb'H:
     *  '00'B the ISORE+ data isn't shown on the DTCO display.
     *  '01'B the ISORE+ data is shown on the DTCO display
     *  '10'B Error indicator
     *  '11'B Not available or not installed
     * 'cc'H:
     *  '00'B the ISORE+ data is not available via diagnostic interface.
     *  '01'B the ISORE+ data is available via diagnostic interface
     *  '10'B Error indicator
     *  '11'B Not available or not installed
     * 'dd'H:
     *  '00'B the ISORE+ warnings are not available via Driver1TimeRelatedStates / Driver2TimeRelatedStates within TCO1 message, diagnostic interface and info interface.
     *  '01'B the ISORE+ warnings are available via Driver1TimeRelatedStates / Driver2TimeRelatedStates within TCO1 message, diagnostic interface and info interface.
     *  '10'B Error indicator
     *  '11'B Not available or not installed
     * 'ee'H:
     *  '01'B the feature "Multi-purpose data recorder" is enabled. '10'B Error indicator
     *  '11'B Not available or not installed
     *  '01'B the VDO1 diagnostic data record is available via diagnostic interface. '10'B Error indicator
     *  '11'B Not available or not installed
     * If the client set a value within OptionalFeaturesCfg to "Error indicator" or "Not available / not installed" then the DTCO shall set such value to the default value.
     * 'ff'H the VDO1 diagnostic data record
        '01'B the VDO1 diagnostic data record is available via diagnostic interface. '10'B Error indicator
        '11'B Not available or not installed
     */

    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class OptionalFeaturesCfgType : SmartLinkCmdDataType
    {
        public int aa;
        public int bb;
        public int cc;
        public int dd;
        public int ee;
        public int ff;

        public OptionalFeaturesCfgType(byte[] b)
        {
            aa = (int)IS_Info.NOT_AVAILABLE;
            dd = (int)IS_Info.NOT_AVAILABLE;
            cc = (int)IS_Info.VALUE0;
            bb = (int)IS_Info.VALUE0;
            ee = (int)IS_Info.VALUE0;
            ff = (int)IS_Info.VALUE0;
            if (b.Length >= 4)
            {
                bool[] bits = SmartLinkHelpers.byteArray2BitArray(b);
                aa = (int)getStatus(bits[20], bits[21]);
                bb = (int)getStatus(bits[22], bits[23]);
                cc = (int)getStatus(bits[24], bits[25]);
                dd = (int)getStatus(bits[26], bits[27]);
                ee = (int)getStatus(bits[28], bits[29]);
                ff = (int)getStatus(bits[30], bits[31]);
            }
        }




        public bool isError(int f)
        {
            return f == (int)IS_Info.ERROR;
        }

        public bool isNotAvailOrNotInstalled(int f)
        {
            return f == (int)IS_Info.NOT_AVAILABLE;
        }
    }
}