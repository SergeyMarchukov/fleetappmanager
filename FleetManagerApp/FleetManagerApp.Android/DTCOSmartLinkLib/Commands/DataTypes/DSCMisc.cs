namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class DSCMisc : SmartLinkCmdDataType
    {
        public bool breakAfterTheTerminalB3IsDetected = false;
        public bool terminalD3ExternallyPowered = false;
        public bool printerPaperIsPresent = false;
        public bool printerDrawerIsOpen = false;
        public bool DTCOHousingOpeningDetectionSensorDetectsOpen = false;
        public bool inputD2IsActive = false;
        public bool inputD1IsActive = false;

        public DSCMisc(byte o)
        {
            breakAfterTheTerminalB3IsDetected = bitIsSet(o, 0);
            terminalD3ExternallyPowered = bitIsSet(o, 2);
            printerPaperIsPresent = bitIsSet(o, 3);
            printerDrawerIsOpen = bitIsSet(o, 4);
            DTCOHousingOpeningDetectionSensorDetectsOpen = bitIsSet(o, 5);
            inputD2IsActive = bitIsSet(o, 6);
            inputD1IsActive = bitIsSet(o, 7);
        }
    }
}