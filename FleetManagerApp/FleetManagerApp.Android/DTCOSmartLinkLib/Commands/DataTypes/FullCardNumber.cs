using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class FullCardNumber : SmartLinkCmdDataType
    {
        public enum EquipmentType
        {
            Unknown = 0,
            DriverCard = 1,
            WorkshopCard = 2,
            ControlCard = 3,
            CompanyCard = 4,
            ManufacturingCard = 5,
            VehicleUnit = 6,
            MotionSensor = 7        
        }

        public EquipmentType? cardTypeExt;
        public int cardType;
        public int cardIssuingMemberState;
        public CardNumber cardNumber;

        public FullCardNumber(byte[] b)
        {
            cardType = SmartLinkHelpers.byte2uint32(b[0]);
            cardTypeExt = getCardType(cardType);
            if (cardTypeExt != null)
            {
                cardIssuingMemberState = SmartLinkHelpers.byte2uint32(b[1]);
                if (b.Length > 2)
                {
                    byte[] bb = new byte[b.Length - 2];
                    for (int i = 2; i < b.Length; i++)
                    {
                        bb[i - 2] = b[i];
                    }
                    cardNumber = new CardNumber(cardTypeExt, bb);
                }
            }

        }

        public static EquipmentType? getCardType(int i)
        {
            foreach (EquipmentType e in Enum.GetValues(typeof(EquipmentType)))
            {
                if (i == (int)e)
                {
                    return e;
                }
            }
            return null;
        }
    }
}