using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Create by Sergey M. 14.03.17
    /// </summary>
    public class ConfigurationValues : SmartLinkCmdDataType
    {
        private readonly static ConfigurationValues _instance = new ConfigurationValues();

        public string serialNumber;
        public string firmwareVersion;
        public DateTime firmwareDate;
        public DateTime firmwareLastUpdateDate;
        public string appleChipInfo;
        public DateTime productionDate;
        public string hardwareVersion;
        public string bluetoothPin;
        public string debugInfo;

        public static ConfigurationValues getInstance()
        {
            return _instance;
        }

        private ConfigurationValues()
        {

            serialNumber = "";
            firmwareVersion = "";
            firmwareDate = new DateTime();
            firmwareLastUpdateDate = new DateTime();
            appleChipInfo = "";
            productionDate = new DateTime();
            bluetoothPin = "";
            debugInfo = "";
            hardwareVersion = "";
        }
    }
}