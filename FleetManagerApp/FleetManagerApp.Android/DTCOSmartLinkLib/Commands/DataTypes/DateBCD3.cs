using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class DateBCD3 : SmartLinkCmdDataType
    {
        public int year;
        public int month;
        public int day;
        public int yearBase;

        public DateBCD3()
        {
            yearBase = 2000;
            year = yearBase;
            month = 1;
            day = 1;
        }
        public DateBCD3(byte[] b)
        {
            yearBase = 2000;
            if (b.Length >= 3)
            {
                year = SmartLinkHelpers.bcdByteToInt(b[0]);
                month = SmartLinkHelpers.bcdByteToInt(b[1]);
                day = SmartLinkHelpers.bcdByteToInt(b[2]);
            }
        }

        public DateTime? getDate()
        {
            try
            {
                //Calendar cal = Calendar.getInstance();
                //cal.set(yearBase + year, month - 1, day);
                //return cal.getTime();

                DateTime dt = new DateTime(yearBase + year, month - 1, day);
                return dt;
            }
            catch (Exception e)
            {
            }

            return null;
        }
    }
}