namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Create by Sergey M. 14.03.17
    /// </summary>
    public enum IS_Info
    {
        VALUE0 = 0,
        VALUE1 = 1,
        ERROR = 2,
        NOT_AVAILABLE = 3
    }
}