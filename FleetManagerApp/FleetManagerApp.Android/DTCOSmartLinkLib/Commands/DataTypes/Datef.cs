namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class Datef : DateBCD3
    {
        public Datef(byte[] b) : base()
        {
            yearBase = 0;
            if (b.Length >= 4)
            {
                year = SmartLinkHelpers.bcdByteToInt(new byte[] { b[0], b[1] });
                month = SmartLinkHelpers.bcdByteToInt(b[2]);
                day = SmartLinkHelpers.bcdByteToInt(b[3]);
            }
        }
    }
}