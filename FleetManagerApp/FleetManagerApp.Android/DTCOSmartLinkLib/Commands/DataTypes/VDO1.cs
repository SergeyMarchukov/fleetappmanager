using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class VDO1 : SmartLinkCmdDataType
    {
        /**
     * Remaining Current Driving Time of the Driver
     *
     * @unit Minute
     *
     * It cannot be greater than 4 hours 30 minutes
     *
     * Placed under Current Tab
     */
        public int remainingCurrentDrivingTime = 0;


        /**
         *Time before start of Next Driving Period
         *
         * Placed Under Current Tab
         *
         *@unit Minute
         */
        public int timeBeforeStartOfNextDrivingPeriod = 0;

        /**
         *Duration Of Next Driving Period
         *
         * @unit Minutes
         *
         * Placed under Current Tab
         *
         */
        public int durationOfNextDrivingPeriod = 0;

        /**
         * Remaining Current Daily Driving Time
         *
         * @Unit Minutes
         *
         * @Max 10 hours => 4.5 + 4.5 + 1
         *
         * 10 hours not on all days
         *
         * Placed under Daily Tab
         */
        public int remainingCurrentDailyDrivingTime = 0;


        /**
         * Remaining Driving Time Of Current Week
         *
         * @Unit Minute
         *
         * placed under weekly tab
         */
        public int remainingDrivingTimeOfCurrentWeek = 0;


        /**
         * Remaining Duration of Current Break/Rest in minutes
         *
         * If its break then 00:45 minutes else the value is greater
         *
         *
         * @unit Minute
         *
         * Placed under current Tab
         */
        public int remainingDurationOfCurrentBreakRest = 0;


        /**
         * Remaining time till next rest/break
         *
         * @Unit Minutes
         *
         * Placed under current tab
         *
         */
        public int remainingTimeTillNextRestBreak = 0;

        /**
         *Duration of Next Rest Break
         *
         *@Units Minutes
         *
         *Placed under current tab
         */
        public int durationOfNextRestBreak = 0;

        /**
         * Remaining time till next daily rest period
         *
         * @Unit Minutes
         *
         * Placed under Daily Tab
         */
        public int remainingTimeTillNextDailyRestPeriod = 0;

        /**
         * Duration of Next Daily Rest period
         *
         * @Unit Minutes
         *
         * Placed Under daily tab
         */
        public int durationOfNextDailyRestPeriod = 0;

        /**
         *Remaining Time Till Next Weekly Rest Period
         *
         * @Unit Minute
         *
         * Placed under weekly tab
         */
        public int remainingTimeTillNextWeeklyRestPeriod = 0;


        /**
         * Duration of Next Weekly Rest Period
         *
         * @unit Minute
         *
         * Placed under weekly tab
         */
        public int durationOfNextWeeklyRestPeriod = 0;

        /**
         * Open Compensation in the Last Week
         *
         * @Unit Minutes
         *
         * placed under weekly tab
         */
        public int openCompensationInTheLastWeek = 0;

        /**
         * Open Compensation in the Week before last
         *
         * @Unit Minutes
         *
         * placed under weekly tab
         */
        public int openCompensationInTheWeekBeforeLast = 0;



        /**
         * Open Compensation in the Second Week before last
         *
         * @Unit Minutes
         *
         * placed under weekly tab
         */
        public int openCompensationInTheSecondWeekBeforeLast = 0;

        /**
         * Number of extended driving days
         *
         *  Max value can be 2
         */
        public int extendedDrivingDays = 0;

        /**
         * Number Of Reduced Rests
         *
         * Max value can be 3
         */
        public int reducedRests;

        public VDO1()
        {
        }

        public VDO1(byte[] b)
        {
            int[] data = SmartLinkHelpers.byteArray2UInt32Array(b, 32);
            remainingCurrentDrivingTime = data[0];
            remainingTimeTillNextRestBreak = data[1];
            durationOfNextRestBreak = data[2];
            remainingDurationOfCurrentBreakRest = data[3];

            timeBeforeStartOfNextDrivingPeriod = data[4];
            durationOfNextDrivingPeriod = data[5];
            remainingCurrentDailyDrivingTime = data[6];
            remainingTimeTillNextDailyRestPeriod = data[7];

            durationOfNextDailyRestPeriod = data[8];
            remainingDrivingTimeOfCurrentWeek = data[9];
            remainingTimeTillNextWeeklyRestPeriod = data[10];
            durationOfNextWeeklyRestPeriod = data[11];

            openCompensationInTheLastWeek = data[12];
            openCompensationInTheWeekBeforeLast = data[13];
            openCompensationInTheSecondWeekBeforeLast = data[14];
            int d = SmartLinkHelpers.byte2uint32(b[31]);
            reducedRests = (d & 0x07);
            extendedDrivingDays = (d & 0x38) >> 3;


        }

        public static VDO1 fromDTCOResponseString(String replyData)
        {
            VDO1 VDO1 = new VDO1();

            VDO1.remainingCurrentDrivingTime = SmartLinkHelpers.getMinutes(replyData, 6, 10);
            VDO1.remainingTimeTillNextRestBreak = SmartLinkHelpers.getMinutes(replyData, 10, 14);
            VDO1.durationOfNextRestBreak = SmartLinkHelpers.getMinutes(replyData, 14, 18);
            VDO1.remainingDurationOfCurrentBreakRest = SmartLinkHelpers.getMinutes(replyData, 18, 22);

            VDO1.timeBeforeStartOfNextDrivingPeriod = SmartLinkHelpers.getMinutes(replyData, 22, 26);
            VDO1.durationOfNextDrivingPeriod = SmartLinkHelpers.getMinutes(replyData, 26, 30);
            VDO1.remainingCurrentDailyDrivingTime = SmartLinkHelpers.getMinutes(replyData, 30, 34);
            VDO1.remainingTimeTillNextDailyRestPeriod = SmartLinkHelpers.getMinutes(replyData, 34, 38);

            VDO1.durationOfNextDailyRestPeriod = SmartLinkHelpers.getMinutes(replyData, 38, 42);
            VDO1.remainingDrivingTimeOfCurrentWeek = SmartLinkHelpers.getMinutes(replyData, 42, 46);
            VDO1.remainingTimeTillNextWeeklyRestPeriod = SmartLinkHelpers.getMinutes(replyData, 46, 50);
            VDO1.durationOfNextWeeklyRestPeriod = SmartLinkHelpers.getMinutes(replyData, 50, 54);

            VDO1.openCompensationInTheLastWeek = SmartLinkHelpers.getMinutes(replyData, 54, 58);
            VDO1.openCompensationInTheWeekBeforeLast = SmartLinkHelpers.getMinutes(replyData, 58, 62);
            VDO1.openCompensationInTheSecondWeekBeforeLast = SmartLinkHelpers.getMinutes(replyData, 62, 66);

            try
            {
                VDO1.reducedRests = Convert.ToInt32(
                        SmartLinkHelpers.hexToBin(replyData.Substring(66)).Substring(10, 13),
                        2);
                VDO1.extendedDrivingDays = Convert.ToInt32(
                        SmartLinkHelpers.hexToBin(replyData.Substring(66)).Substring(13), 2);
            }
            catch (Exception e)//(NumberFormatException e)
            {

            }

            return VDO1;
        }
    }
}