namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public enum NationalNumeric
    {
        NO_INFORMATION = 0,
        Austria = 0x01,
        Albania = 0x02,
        Andorra = 0x03,
        Armenia = 0x04,
        Azerbaijan = 0x05,
        Belgium = 0x06,
        Bulgaria = 0x07,
        BosniaAndHerzegovina = 0x08,
        Belarus = 0x09,
        Switzerland = 0x0a,
        Cyprus = 0x0b,
        CzechRepublic = 0xc,
        Germany = 0x0d,
        Denmark = 0x0e,
        Spain = 0x0f,
        Estonia = 0x10,
        France = 0x11,
        Finland = 0x12,
        Liechtenstein = 0x13,
        FaeroeIslands = 0x14
    }
}