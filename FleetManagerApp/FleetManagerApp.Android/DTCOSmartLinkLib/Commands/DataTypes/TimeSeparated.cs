using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class TimeSeparated : SmartLinkCmdDataType
    {
        public int seconds;
        public int minutes;
        public int hours;
        public int month;
        public int day;
        public int year;
        public int localMinuteOffset;
        public int localHourOffset;
        public DateTime date;

        public TimeSeparated(byte[] b)
        {
            seconds = 0;
            minutes = 0;
            hours = 0;
            month = 1;
            day = 1;
            year = 1985;
            localMinuteOffset = 0;
            localHourOffset = 0;
            date = new DateTime();

            if (b.Length >= 8)
            {
                int[] bi = SmartLinkHelpers.byteToUnsignedInt(b);
                seconds = bi[0] >> 2;
                minutes = bi[1];
                hours = bi[2];
                month = bi[3];
                day = (bi[4] >> 2) + 1;
                year = 1985 + bi[5];
                localMinuteOffset = bi[6] - 125;
                localHourOffset = bi[7] - 125;
                //TimeZone zone = TimeZone.getTimeZone("GMT");          // Commented by SM.
                //Calendar cal = Calendar.getInstance(zone);
                //cal.set(year, month - 1, day, hours, minutes, seconds);

                ////cal.set(Calendar.ZONE_OFFSET,(localHourOffset*3600+localMinuteOffset*60)*1000);
                //date = cal.getTime();
            }
        }
    }
}