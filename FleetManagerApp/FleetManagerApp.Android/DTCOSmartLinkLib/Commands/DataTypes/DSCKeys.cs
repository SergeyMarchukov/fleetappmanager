namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class DSCKeys : SmartLinkCmdDataType
    {
        bool selectOKMenuButtonPressed = false;
        bool backMenuButtonPressed = false;
        bool upMenuButtonPressed = false;
        bool downMenuButtonPressed = false;
        bool driver2ActivitySettingsButtonPressed = false;
        bool driver1ActivitySettingsButtonPressed = false;
        bool smartCartSlot2WithdrawalRequestButtonPressed = false;
        bool smartCartSlot1WithdrawalRequestButtonPressed = false;

        public DSCKeys(byte o)
        {
            selectOKMenuButtonPressed = bitIsSet(o, 0);
            backMenuButtonPressed = bitIsSet(o, 1);
            upMenuButtonPressed = bitIsSet(o, 2);
            downMenuButtonPressed = bitIsSet(o, 3);
            driver2ActivitySettingsButtonPressed = bitIsSet(o, 4);
            smartCartSlot2WithdrawalRequestButtonPressed = bitIsSet(o, 5);
            driver1ActivitySettingsButtonPressed = bitIsSet(o, 6);
            smartCartSlot1WithdrawalRequestButtonPressed = bitIsSet(o, 7);
        }
    }
}