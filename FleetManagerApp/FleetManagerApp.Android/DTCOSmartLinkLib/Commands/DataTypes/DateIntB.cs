using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.DataTypes
{
    /// <summary>
    /// Created by Sergey M. 14.03.17
    /// </summary>
    public class DateIntB : SmartLinkCmdDataType
    {
        public int year;
        public int month;
        public int day;
        protected int yearBase;

        public DateIntB()
        {
            yearBase = 1985;
            year = yearBase;
            month = 1;
            day = 1;
        }
        public DateIntB(byte[] b)
        {
            yearBase = 1985;
            if (b.Length >= 3)
            {

                year = SmartLinkHelpers.byte2uint32(b[2]);
                month = SmartLinkHelpers.byte2uint32(b[0]) & 0x0F;
                day = SmartLinkHelpers.byte2uint32(b[1]) & 0x1F;
            }
        }

        public DateTime? getDate()
        {
            try
            {
                //Calendar cal = Calendar.getInstance();
                //cal.set(yearBase + year, month - 1, day);
                //return cal.getTime();

                DateTime dt = new DateTime(yearBase + year, month - 1, day);
                return dt;
            }
            catch (Exception e)
            {
            }
            return null;
        }
    }
}