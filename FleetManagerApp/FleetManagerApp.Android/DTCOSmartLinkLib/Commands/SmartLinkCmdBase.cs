using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands
{
    public abstract class SmartLinkCmdBase
    {
        public byte GetChecksum(byte[] byteArray)
        {
            int cs = 0;

            for (int i = 0; i < byteArray.Length; i++)
            {
                cs = cs ^ byteArray[i];
            }

            int d = cs & 0xff;
            //return String.format("%02X", d);
            return Convert.ToByte(d);
        }
    }
}