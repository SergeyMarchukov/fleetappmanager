using System;
using FleetManagerApp.Extensions;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands
{
    /// <summary>
    /// Created by Sergey M. 13.03.17
    /// </summary>
    public class SmartLinkCmd : ISmartLinkOperationCallback
    {
        protected string mCommandName;
        protected ISmartLinkOperationCallback _commandCallback;
        protected string _hexCommandCode;
        protected string _hexResponse;

        protected string DTCO_COMMAND_NEGATIVE_RESPONSE;
        protected string DTCO_SEND_COMMAND_PREFIX;
        protected string DTCO_RECV_COMMAND_PREFIX;

        protected int _interface;
        protected SmartLinkOperationResult _lastResult;
        protected SmartLinkMessage _lastSmartLinkMessage;
        protected SmartLinkLogger smartLinkLogger;

        protected SmartLinkCmd(ISmartLinkOperationCallback callback)
        {
            _commandCallback = callback;
            DTCO_COMMAND_NEGATIVE_RESPONSE = "";
            DTCO_SEND_COMMAND_PREFIX = "";
            DTCO_RECV_COMMAND_PREFIX = "";
            _lastResult = null;
            mCommandName = "";
            _hexCommandCode = "";
            _hexResponse = "";
            _interface = -1;
            smartLinkLogger = null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iface">SmartLink Interface number</param>
        /// <param name="command">hex command string eg. "22FD01"</param>
        /// <param name="callback">SmartLinkOperationCallback as operation result getter</param>
        /// <returns>SmartLinkCmd</returns>
        public static SmartLinkCmd Factory(int iface, string command, ISmartLinkOperationCallback callback)
        {
            SmartLinkCmd cmd = new SmartLinkCmd(callback);
            cmd.mCommandName = command;
            cmd._hexCommandCode = command;
            cmd._hexResponse = command;
            cmd._interface = iface;
            return cmd;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iface">SmartLink Interface number</param>
        /// <param name="command">hex command string eg. "22FD01"</param>
        /// <param name="responseNeedle">hex command string eg. "62FD01" for result prefix needle</param>
        /// <param name="callback">SmartLinkOperationCallback as operation result getter</param>
        /// <returns>SmartLinkCmd</returns>
        public static SmartLinkCmd Factory(int iface, string command, string responseNeedle, ISmartLinkOperationCallback callback)
        {
            SmartLinkCmd cmd = SmartLinkCmd.Factory(iface, command, callback);
            cmd._hexResponse = responseNeedle;
            return cmd;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args">mixed args for command execution</param>
        /// <returns>SmartLinkCmd</returns>
        public SmartLinkCmd execute(params Object[] args)
        {
            if (smartLinkLogger != null)
            {
                smartLinkLogger.writeStartCommand(this);
            }
            SmartLinkBluetoothSerial SL = SmartLinkBluetoothSerial.getInstance();
            SL.executeCommand(this);
            return this;
        }

        /// <summary>
        /// Getter for needle response
        /// </summary>
        /// <returns>string</returns>
        public string needleResponse()
        {
            return _hexResponse;
        }

        /// <summary>
        /// Return always true. Have to override in command class
        /// </summary>
        /// <param name="msg">SmartLinkMessage response from smart link</param>
        /// <returns>bool</returns>
        public bool isMyResponse(SmartLinkMessage msg)
        {
            return true;
        }

        /// <summary>
        /// Return always false. Have to override in command class
        /// </summary>
        /// <param name="msg">SmartLinkMessage response from smart link</param>
        /// <returns>bool</returns>
        public bool isErrorReply(SmartLinkMessage msg)
        {
            return false;
        }

        protected void Log(string message)
        {
            Android.Util.Log.Debug("SmartLinkCommand", string.Format("%s: %s", getCommandName(), message));
        }

        public void setSmartLinkLogger(SmartLinkLogger l)
        {
            smartLinkLogger = l;
        }

        /// <summary>
        /// Return the las result of operation
        /// </summary>
        /// <returns>SmartLinkOperationResult</returns>
        public SmartLinkOperationResult getLastResult()
        {
            return _lastResult;
        }

        /// <summary>
        /// Parse response message. Have to override in command implementation.
        /// Return JSON representation of SmartLinkMessage
        /// </summary>
        /// <param name="msg">SmartLinkMessage response from smart link</param>
        /// <returns>Object</returns>
        protected Object parseCommandResponse(SmartLinkMessage msg)
        {
            try
            {
                return msg.toJSON();
            }
            catch (Exception e)//(JSONException e)
            {
                //e.printStackTrace();
            }
            return null;
        }

        /// <summary>
        /// Getter for the human readable command representation
        /// </summary>
        /// <returns>string</returns>
        public string getCommandName()
        {
            return string.IsNullOrEmpty(mCommandName) ? _hexCommandCode : mCommandName;
        }

        /// <summary>
        /// For String representation
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            return getCommandName();
        }

        /// <summary>
        /// Return hex string command
        /// </summary>
        /// <returns>string</returns>
        public string getCommand()
        {
            return _hexCommandCode;
        }

        /// <summary>
        /// Return string message for sending to smart link
        /// </summary>
        /// <returns>string</returns>
        public string getCommandMessage()
        {
            return DTCO_SEND_COMMAND_PREFIX + getCommand();
        }

        /// <summary>
        /// Getter fro command smart link interface
        /// </summary>
        /// <returns>int</returns>
        public int getInterface()
        {
            return _interface;
        }

        public SmartLinkMessage getlastSmartLinkMessage()
        {
            return _lastSmartLinkMessage;
        }

        protected SmartLinkNegativeResponse? getNegativeResponse(SmartLinkMessage msg)
        {
            try
            {
                int i = Convert.ToByte(msg.getData().Substring(4, 6), 16);
                return (SmartLinkNegativeResponse)i;

            }
            catch (Exception e)
            {

            }

            return null;
        }

        /// <summary>
        /// Inherits from SmartLinkOperationCallback.
        /// Parsed smart link command result
        /// </summary>
        /// <param name="result"></param>
        public void onSmartLinkOperationResult(SmartLinkOperationResult result)
        {
            _lastSmartLinkMessage = null;
            _lastResult = null;
            if (result.is_ok())
            {
                SmartLinkMessage msg = result.getData() as SmartLinkMessage;
                if (msg != null)
                {
                    _lastSmartLinkMessage = msg;
                    if (isErrorReply(msg))
                    {
                        result.negativeResponse = getNegativeResponse(msg);
                        if (result.negativeResponse == null)
                        {
                            result.setError(SmartLinkErrorCode.DTCO_COMMAND_NEGATIVE_RESPONSE);
                        }
                        else
                        {
                            result.setError(result.negativeResponse.GetDescription(), SmartLinkErrorCode.DTCO_COMMAND_NEGATIVE_RESPONSE);
                        }

                    }
                    else
                    {
                        if (isMyResponse(msg))
                        {
                            result.setData(parseCommandResponse(msg));
                        }
                        else
                        {

                            try
                            {
                                result.setData(msg.toJSON());
                            }
                            catch (Exception e)//(JSONException e)
                            {
                                //e.printStackTrace();
                                Log(e.StackTrace);
                            }
                        }
                    }
                }
                Log(result.getData().ToString());
            }
            else
            {
                Log(String.Format("Error. Code: %s. Message: %s", result.error_code.ToString(), result.message));
            }
            result.cmd = this;

            if (_commandCallback != null)
            {
                _commandCallback.onSmartLinkOperationResult(result);
            }
            _lastResult = result;

            if (smartLinkLogger != null)
            {
                smartLinkLogger.writeCommandResult(result);
            }
        }
    }
}