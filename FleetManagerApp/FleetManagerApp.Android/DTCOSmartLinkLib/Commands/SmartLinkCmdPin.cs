using FleetManagerApp.BTInterfaces;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Enums;
using System;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency(typeof(FleetManagerApp.Droid.DTCOSmartLinkLib.Commands.SmartLinkCmdPin))]
namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands
{
    public class SmartLinkCmdPin : SmartLinkCmdBase, ISmartLinkCmd
    {
        private byte RFU = 0xFF;

        public SmartLinkInterfaceEnum SmartLinkInterface { get; private set; } = SmartLinkInterfaceEnum.SESSION_HANDLING;

        public int SessionToken { get; private set; }

        public int DataLength { get; private set; }

        public byte[] Data { get; private set; }

        public int CheckSum { get; private set; }

        public byte[] Message { get; private set; }
        
        public SmartLinkCmdPin(int sessionToken, byte[] data)
        {
            if (data.Length > 0)
            {
                List<byte> mData = new List<byte>();
                mData.Add((byte)SmartLinkInterface);
                mData.Add(Convert.ToByte(sessionToken));
                mData.Add(RFU);
                mData.Add(Convert.ToByte((data.Length / 2).ToString("X4")));
                mData.Add(GetChecksum(data));

                Message = mData.ToArray();
            }
        }

        public byte[] GetBytes()
        {
            return Message;
        }
    }
}