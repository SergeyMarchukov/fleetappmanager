using FleetManagerApp.Droid.DTCOSmartLinkLib;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Commands;
using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Commands
{
    /// <summary>
    /// Created by Sergey M. 13.03.17
    /// </summary>
    public abstract class SmartLinkCmdRDBI : SmartLinkCmd
    {
        public enum SmartLinkCmdRDBIDictionary
        {
            DigitalSensorsCalis = 0xFD00,
            AdjustLocalHourOffset = 0xF90E,
            AdjustLocalMinuteOffset = 0xF90D,
            Driver1Name = 0xF931,
            Driver2Name = 0xF932,
            TachographVehicleSpeed = 0xF902,
            Voltage = 0xFD01,
            TimeDate = 0xF90B,
            VehicleRegistrationNumber = 0xF97E,
            Driver1Identification = 0xF916,
            Driver2Identification = 0xF917,
            VDO1 = 0xFD8D,
            VehicleIdentificationNumber = 0xF190,
            D1D2Cfg = 0xFD16,
            AnalogSensors = 0xFD01,
            CalibrationDate = 0xF19B,
            ChgActByIgn = 0xFE17,
            Driver1EndOfLastDailyRestPeriod = 0xF997,
            Driver1EndOfLastWeeklyRestPeriod = 0xF998,
            Driver1EndOfSecondLastWeeklyRestPeriod = 0xF999,
            Driver1CurrentDailyDrivingTime = 0xF99A,
            Driver1CurrentWeeklyDrivingTime = 0xF99B,
            Driver1TimeLeftUntilNewRestPeriod = 0xF99C,
            Driver1NumberOfTimes9hDailyDrivingTimesExceeded = 0xF9A0,
            Driver1ContinuousDrivingTime = 0xF923,
            Driver1CumulatedDrivingTimePreviousAndCurrentWeek = 0xF938,
            Driver1CumulativeBreakTime = 0xF925,
            Driver1CurrentDurationOfSelectedActivity = 0xF927,
            Driver1WorkingState = 0xF903,
            Driver1TimeRelatedStates = 0xF906,
            Driver2ContinuousDrivingTime = 0xF924,
            Driver2CumulatedDrivingTimePreviousAndCurrentWeek = 0xF939,
            Driver2CumulativeBreakTime = 0xF926,
            Driver2CurrentDurationOfSelectedActivity = 0xF928,
            Driver2TimeRelatedStates = 0xF909,
            Driver2WorkingState = 0xF904,
            DriverCardDriver1 = 0xF907,
            DriverCardDriver2 = 0xF90A,
            DriveRecognise = 0xF905,
            ECUInstallationDate = 0xF19D,
            ECUManufacturingDate = 0xF18B,
            ECUSerialNumber = 0xF18C,
            EngineSpeed = 0xF95A,
            FullCardIdentificationSlot1 = 0xFE04,
            FullCardIdentificationSlot2 = 0xFE05,
            HighResolutionTripDistance = 0xF913,
            HWVariant = 0xFE10,
            IMSSpeed = 0xFDA0,
            InternalStateTestInformation = 0xFD02,
            ModeOfOperation = 0xF937,
            OptionalFeaturesCfg = 0xFD87,
            NextMandatoryDriverCardDownloadPrewarningTime = 0xFD36,
            OutOfScopeCondition = 0xF936,
            OverSpeed = 0xF908,
            PreferredLanguage = 0xFE02,
            PreOverspeedAlert = 0xFE03,
            PressButtonCfg = 0xFE13,
            PrinterDrawerState = 0xFE00,
            RemoteDownloadCalibrationInterfaceCfg = 0xFD39,
            RemoteDownloadCAN1Config = 0xFD80,
            RemoteDownloadCAN2Config = 0xFD81,
            RemoteDownloadCAN1IgnitionOffConfig = 0xFD97,
            RemoteDownloadCAN2IgnitionOffConfig = 0xFD98,
            SoftInstallationDate = 0xFD12,
            SpeedMeasurementRange = 0xF919,
            SystemSupplierECUHardwareNumber = 0xF192,
            SystemSupplierECUHardwareVersionNumber = 0xF193,
            SystemSupplierECUSoftwareNumber = 0xF194,
            SystemSupplierECUSoftwareVersionNumber = 0xF195,
            SystemSupplierIdentifier = 0xF18A,
            TachographCardSlot1 = 0xF930,
            TachographCardSlot2 = 0xF933,
            TCO3and4 = 0xFD86,
            VoltageVariant = 0xFD0F,
            VehicleManufacturerECUHardwareNumber = 0xF191,
            VehicleRegistrationNation = 0xFD11,
            WarnBeforeExpiryDates = 0xFD31,
            TimeBetweenTwoMandatoryDriverCardDownloads = 0xFD3C,
            HighResolutionTotalVehicleDistance = 0xF912,
            Kfactor = 0xF918,
            LTyreCircumference = 0xF91C,
            WVehicleCharacteristicConstant = 0xF91D,
            TyreSize = 0xF921,
            NextCalibrationDate = 0xF922,
            SpeedAuthorised = 0xF92C,
            RegisteringMemberState = 0xF97D
        }

        protected SmartLinkCmdRDBI(ISmartLinkOperationCallback callback) : base(callback)
        {
            _interface = SmartLinkConstants.IFACE_KLINE;
            DTCO_COMMAND_NEGATIVE_RESPONSE = "7F";
            DTCO_SEND_COMMAND_PREFIX = "22";
            DTCO_RECV_COMMAND_PREFIX = "62";
            _commandCallback = callback;
            mCommandName = "";
            _hexCommandCode = "";
            _hexResponse = "";
        }

        protected void _initValuesFromDictionary(SmartLinkCmdRDBIDictionary k)
        {
            mCommandName = k.ToString();
            _hexCommandCode = Enum.Format(k.GetType(), k, "X"); //k.getHexCode();
            _hexResponse = _hexCommandCode;
        }

        public static SmartLinkCmdRDBI Factory(SmartLinkCmdRDBIDictionary cmd, ISmartLinkOperationCallback callback)// throws SmartLinkCommandClassNotFoundException
        {
            //try           // Commented by SM.
            //{
            //    Class cl = Class.forName("no.cloudconsulting.dtco.commands.SmartLinkCmd" + cmd.ToString());
            //    Constructor con = cl.getConstructor(ISmartLinkOperationCallback.class);
            //    Object _c = con.newInstance(callback);
            //    if(_c instanceof SmartLinkCmdRDBI){
            //        return (SmartLinkCmdRDBI) _c;
            //    }
            //    throw new SmartLinkCommandClassNotFoundException("Not Kline command.");
            //} 
            //catch (Exception e)
            //{
            //    throw new SmartLinkCommandClassNotFoundException(e.Message);
            //}
            return null;
        }
    
        //@Override
        public SmartLinkCmdRDBI execute(params Object[] args)
        {
            string _origCommand = _hexCommandCode;
            if (args.Length > 0)
            {
                if (args[0] is string){
                    _hexCommandCode += args[0].ToString();
                }
            }
            SmartLinkCmdRDBI cmd = (SmartLinkCmdRDBI)base.execute();
            _hexCommandCode = _origCommand;
            return cmd;
        }

        //@Override
        public bool isMyResponse(SmartLinkMessage msg)
        {
            //try       // Commented by SM.
            //{
            //    return msg.getData().startsWith(DTCO_RECV_COMMAND_PREFIX + _hexResponse);
            //}
            //catch (Exception e)
            {
                return false;
            }
        }

        //@Override
        public bool isErrorReply(SmartLinkMessage msg)
        {
            //if (DTCO_COMMAND_NEGATIVE_RESPONSE.length() > 0)      // Commented by SM.
            //{
            //    return msg.getData().startsWith(DTCO_COMMAND_NEGATIVE_RESPONSE + DTCO_SEND_COMMAND_PREFIX);
            //}
            return false;
        }
    }
}