using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Exceptions
{
    /// <summary>
    /// Created by Sergey M. 13.03.17
    /// </summary>
    public class SmartLinkException : Exception
    {
        public SmartLinkException(string s) : base(s)
        { }
    }
}