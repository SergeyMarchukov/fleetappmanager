namespace FleetManagerApp.Droid.DTCOSmartLinkLib.Exceptions
{
    /// <summary>
    /// Created by Sergey M. 13.03.17
    /// </summary>
    public class SmartLinkCommandClassNotFoundException : SmartLinkException
    {
        public SmartLinkCommandClassNotFoundException(string s) : base(s)
        { }
    }
}