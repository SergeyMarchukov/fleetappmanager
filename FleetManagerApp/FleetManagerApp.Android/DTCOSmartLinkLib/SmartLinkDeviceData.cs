using Android.Bluetooth;
using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    public class SmartLinkDeviceData
    {
        public String description;
        public String mac;
        BluetoothDevice device;

        public SmartLinkDeviceData(BluetoothDevice d)
        {
            device = d;
            description = d.Name;
            mac = d.Address;
        }

        public string getSpinnerText()
        {
            return description;
        }

        public string getValue()
        {
            return mac;
        }

        public override string ToString()
        {
            return description;
        }
        
    }
}