using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    public class SmartLinkHelpers
    {
        public static readonly string EMPTY = "";
        public static readonly string HEX_TO_BIN_UNWANTED = "0x";
        public static readonly string COUNT_VALUE = "0";

        public static string checkAndConvertPassword(string password)
        {
            string strReturnValue = "";

            if (password.matches("\\d+") == true)
            {
                for (int intLoop = 0; intLoop <= password.Length - 1; intLoop++)
                {
                    char c = password.charAt(intLoop);
                    strReturnValue += Integer.toHexString(c);
                }
            }
            return strReturnValue;
        }

        public static String getChecksum(byte[] byteArray)
        {
            int cs = 0;

            for (int i = 0; i < byteArray.Length; i++)
            {
                cs = cs ^ byteArray[i];
            }

            int i = cs & 0xff;
            return string.Format("%02X", i);
        }

        public static byte[] hexStringToByteArray(string s)
        {
            int len = s.Length;
            if (len % 2 != 0)
            {
                s = "0" + s;
                len += 1;
            }
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2)
            {
                data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(
                        s.charAt(i + 1), 16));
            }
            return data;
        }

        public static int[] hexStringToIntArray(string s)
        {
            int len = s.Length;
            if (len % 2 != 0)
            {
                s = "0" + s;
                len += 1;
            }
            int[] data = new int[len / 2];
            for (int i = 0; i < len; i += 2)
            {
                data[i / 2] = Int32.Parse(s.Substring(i, i + 2), 16);// Integer.parseInt(s.Substring(i, i + 2), 16);
            }
            return data;
        }

        public static string hexToBin(string hex)
        {
            string bin = EMPTY;
            string binFragment = EMPTY;
            int iHex;
            string hexBin = hex;
            hexBin = hexBin.Trim();
            hexBin = hexBin.replaceFirst(HEX_TO_BIN_UNWANTED, EMPTY);
            for (int i = 0; i < hexBin.Length; i++)
            {
                iHex = Integer.parseInt(EMPTY + hexBin.charAt(i), 16);
                binFragment = Integer.toBinaryString(iHex);
                while (binFragment.Length < 4)
                {
                    binFragment = COUNT_VALUE + binFragment;
                }
                bin += binFragment;
            }
            return bin;
        }

        public static string convertToChar(string replyData)
        {
            string asciiValue = EMPTY;

            for (int i = 2; i < replyData.Length; i += 2)
            {
                if ((i + 2) == replyData.Length)
                {
                    asciiValue += (char)Integer.parseInt(replyData.Substring(i), 16);
                }
                else
                {
                    asciiValue += (char)Integer.parseInt(replyData.Substring(i, i + 2), 16);
                }
            }
            return asciiValue;
        }

        public static String getCodePage(int codePage)
        {
            String strCodePage;
            switch (codePage)
            {
                case 2:
                    strCodePage = "ISO-8859-2";
                    break;
                case 3:
                    strCodePage = "ISO-8859-3";
                    break;
                case 5:
                    strCodePage = "ISO-8859-5";
                    break;
                case 7:
                    strCodePage = "ISO-8859-7";
                    break;
                case 9:
                    strCodePage = "ISO-8859-9";
                    break;
                case 13:
                    strCodePage = "ISO-8859-13";
                    break;
                case 15:
                    strCodePage = "ISO-8859-15";
                    break;
                case 16:
                    strCodePage = "Windows-1250";
                    break;
                case 80:
                    strCodePage = "Cyrillic";
                    break;
                case 85:
                    strCodePage = "Cyrillic";
                    break;
                default:
                    strCodePage = "ISO-8859-1";
                    break;
            }

            return strCodePage;
        }


        public static int getMinutes(string fullString, int start, int end)
        {
            return Int32.Parse(fullString.Substring(start, end), 16);
        }

        public static bool isValidForSendMessageInterface(int i)
        {
            return (i >= SmartLinkConstants.IFACE_KLINE && i <= SmartLinkConstants.IFACE_UPGRADE);
        }

        public static int bcdByteToInt(byte b)
        {
            int ret = 0;
            // treat out-of-range BCD values as 0
            if ((b & 0xf0) <= 0x90)
            {
                ret = ((b >> 4) & 0xf) * 10;
            }
            if ((b & 0x0f) <= 0x09)
            {
                ret += (b & 0xf);
            }
            return ret;
        }

        public static int bcdByteToInt(byte[] b)
        {
            int ret = 0;
            try
            {
                for (int i = 0; i < b.Length; i++)
                {
                    ret += (int)(bcdByteToInt(b[i]) * Math.Pow(100, b.Length - i - 1));
                }
            }
            catch (Exception e)
            {

            }
            return ret;
        }

        public static int byteArrayToInt(byte[] b)
        {
            try
            {
                if (b.Length == 4)
                    return (b[0] & 0xff) << 24 | (b[1] & 0xff) << 16 | (b[2] & 0xff) << 8 | (b[3] & 0xff);
                else if (b.Length == 2)
                    return (b[0] & 0xff) << 8 | (b[1] & 0xff);
            }
            catch (Exception e)
            {

            }
            return 0;
        }

        public static int byteArrayToInt(byte[] b, int len)
        {
            return byteArrayToInt(b, len, 0);
        }

        public static int byteArrayToInt(byte[] b, int len, int offset)
        {
            int ret = 0;
            if (b.Length - offset >= len)
            {
                try
                {
                    switch (len)
                    {
                        case 4:
                            return byteArrayToInt(new byte[] { b[offset], b[offset + 1], b[offset + 2], b[offset + 3] });
                        case 2:
                            return byteArrayToInt(new byte[] { b[offset], b[offset + 1] });
                    }
                }
                catch (Exception e)
                {

                }
            }
            return ret;
        }

        public static int[] byteArray2UInt32Array(byte[] b, int len)
        {
            int[] ret = new int[len / 2];
            int j = 0;
            for (int i = 0; i < b.Length - 1; i += 2)
            {
                ret[j] = byteArrayToInt(b, 2, i);
                j++;
            }
            return ret;
        }

        /**
         * Convert a byte array to a boolean array. Bit 0 is represented with false,
         * Bit 1 is represented with 1
         *
         * @param bytes byte[]
         * @return boolean[]
         */
        public static bool[] byteArray2BitArray(byte[] bytes)
        {
            bool[] bits = new bool[bytes.Length * 8];
            for (int i = 0; i < bytes.Length * 8; i++)
            {
                if ((bytes[i / 8] & (1 << (7 - (i % 8)))) > 0)
                    bits[i] = true;
            }
            return bits;
        }

        public static int[] byteToUnsignedInt(byte[] src)
        {
            int[] i_src = new int[src.Length];
            for (int i = 0; i < src.Length; i++) i_src[i] = src[i] < 0 ? src[i] + 256 : src[i];
            return i_src;
        }

        public static int byte2uint32(byte src)
        {
            return (int)(src < 0 ? src + 256 : src);
        }
    }
}