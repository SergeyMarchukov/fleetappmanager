using System;
using System.Text;
using Newtonsoft.Json.Linq;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Enums;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    public class SmartLinkMessage
    {
        private int RFU = 0xFF;

        public SmartLinkInterfaceEnum SmartLinkInterface { get; private set; }

        private int _sessionToken;
        public int SessionToken
        {
            get { return _sessionToken; }
            set
            {
                _sessionToken = value;
                UpdateChecksum();
            }
        }

        public int DataLength { get; private set; }

        public byte[] Data { get; private set; }

        public int CheckSum { get; private set; }

        public byte[] Message { get; private set; }

        // TODO
        public SmartLinkMessage(byte[] message)
        {
            Message = message;

            //mIface = SmartLinkHelpers.byte2uint32(message[0]);
            SmartLinkInterface = (SmartLinkInterfaceEnum)Enum.Parse(typeof(SmartLinkInterfaceEnum), message[0].ToString());

            //mSessionToken = SmartLinkHelpers.byte2uint32(message[1]);
            _sessionToken = message[1];

            //mDataLength = Convert.ToByte(string.Format("%02X", message[3]) + string.Format("%02X", message[4]), 16);
            DataLength = message[3] + message[4];

            //int i;

            //StringBuilder sb = new StringBuilder();

            //StringBuilder sb_raw = new StringBuilder();

            //if (mDataLength > 3)
            //{
            //    mRawMessage = new byte[mDataLength - 3];
            //}

            //for (i = 0; i < mDataLength; i++)
            //{
            //    sb.Append(string.Format("%02X", message[i + 5]));
            //    if (i > 2)
            //    {
            //        mRawMessage[i - 3] = message[i + 5];
            //    }
            //}

            //mData = sb.ToString();

            //mChecksum = SmartLinkHelpers.byte2uint32(message[5 + i]);
        }

        // TODO
        private void UpdateChecksum()
        {
            string s = string.Format("%02X", SmartLinkInterface) + string.Format("%02X", _sessionToken) + "FF"
                    + string.Format("%04X", DataLength) + BitConverter.ToString(Data);

            int len = s.Length;
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2)
            {
                data[i / 2] = (byte)((Java.Lang.Character.Digit(s[i], 16) << 4) + Java.Lang.Character.Digit(s[i + 1], 16));
            }

            int cs = 0;
            for (int i = 0; i < data.Length; i++)
            {
                cs = cs ^ data[i];
            }

            CheckSum = cs & 0xff;
        }

        public override string ToString()
        {
            return "Interface: " + SmartLinkInterface + ", SessionToken: " + string.Format("%02x", _sessionToken)
                    + ", DataLength: " + DataLength + ", Data: " + BitConverter.ToString(Data) + ", Checksum: "
                    + string.Format("%02d", CheckSum);
        }

        public JObject ToJSON()// throws JSONException
        {
            JObject json = new JObject();
            json.Add("iFace", (int)SmartLinkInterface);
            json.Add("sessionToken", _sessionToken);
            json.Add("length", DataLength);
            json.Add("data", BitConverter.ToString(Data));
            json.Add("checksum", string.Format("%02d", CheckSum));
            return json;
        }

        ///////////////

        #region Implemented methods

        //private int _sessionToken;
        //public void setSessionToken(int sessionToken)
        //{
        //    mSessionToken = sessionToken;
        //    UpdateChecksum();
        //}

        //public int getSessionToken()
        //{
        //    return mSessionToken;
        //}

        //private int mIface;
        //public int getIface()
        //{
        //    return mIface;
        //}

        //private int mDataLength;
        //public int getDataLength()
        //{
        //    return mDataLength;
        //}

        //private string mData;
        //public string getData()
        //{
        //    return mData;
        //}

        //private int mChecksum;
        //public int getChecksum()
        //{
        //    return mChecksum;
        //}

        //private byte[] mRawMessage = new byte[0];
        //public byte[] getRawMessage()
        //{
        //    return mRawMessage;
        //}

        #endregion


        // TODO
        public string getHexCheckSum()
        {
            string strCS = string.Format("%02X", CheckSum);
            if (strCS.Length > 2)
            {
                return strCS.Substring(strCS.Length - 2, strCS.Length);
            }
            else
            {
                return strCS;
            }
        }

        // TODO
        public byte[] getByteArray()
        {
            string s = toHexString();

            int len = s.Length;
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2)
            {
                data[i / 2] = (byte)((Java.Lang.Character.Digit(s[i], 16) << 4) + Java.Lang.Character.Digit(s[i + 1], 16));
            }
            return data;
        }

        // TODO
        public string toHexString()
        {
            return string.Format("%02X", SmartLinkInterface) + string.Format("%02X", _sessionToken) + "FF"
                    + string.Format("%04X", DataLength) + BitConverter.ToString(Data) + string.Format("%02X", CheckSum);
        }
        
    }
}