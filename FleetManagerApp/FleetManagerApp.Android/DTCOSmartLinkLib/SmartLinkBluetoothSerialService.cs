using Android.Bluetooth;
using Android.OS;
using Android.Util;
using Java.Util;
using System;
using System.IO;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{

    /// <summary>
    /// 
    /// This class does all the work for setting up and managing Bluetooth
    /// connections with other devices. It has a thread that listens for
    /// incoming connections, a thread for connecting with a device, and a
    /// thread for performing data transmissions when connected.
    /// 
    /// This code was based on the Android SDK BluetoothChat Sample
    /// $ANDROID_SDK/samples/android-17/BluetoothChat
    /// 
    /// </summary>
    public class SmartLinkBluetoothSerialService
    {
        // Debugging
        private static readonly string TAG = "SmartLinkBSS";
        private static readonly bool D = true;

        // Name for the SDP record when creating server socket
        private static readonly string NAME_SECURE = "PhoneGapBluetoothSerialServiceSecure";
        private static readonly string NAME_INSECURE = "PhoneGapBluetoothSerialServiceInSecure";

        // Unique UUID for this application
        private static readonly UUID MY_UUID_SECURE = UUID.FromString("7A9C3B55-78D0-44A7-A94E-A93E3FE118CE");
        private static readonly UUID MY_UUID_INSECURE = UUID.FromString("23F18142-B389-4772-93BD-52BDBB2C03E9");

        // Well known SPP UUID
        private static readonly UUID UUID_SPP = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");

        // Member fields
        private readonly BluetoothAdapter mAdapter;
        private readonly Handler mHandler;
        private AcceptThread mSecureAcceptThread;
        private AcceptThread mInsecureAcceptThread;
        private ConnectThread mConnectThread;
        private ConnectedThread mConnectedThread;
        private int mState;

        // Constants that indicate the current connection state
        public static readonly int STATE_NONE = 0;       // we're doing nothing
        public static readonly int STATE_LISTEN = 1;     // now listening for incoming connections
        public static readonly int STATE_CONNECTING = 2; // now initiating an outgoing connection
        public static readonly int STATE_CONNECTED = 3;  // now connected to a remote device


        /// <summary>
        /// Constructor. Prepares a new BluetoothSerial session.
        /// </summary>
        /// <param name="handler">A Handler to send messages back to the UI Activity</param>
        public SmartLinkBluetoothSerialService(Handler handler)
        {
            mAdapter = BluetoothAdapter.DefaultAdapter;
            mState = STATE_NONE;
            mHandler = handler;
        }

        /// <summary>
        /// Set the current state of the chat connection
        /// </summary>
        /// <param name="state">An integer defining the current connection state</param>
        private /*synchronized*/ void setState(int state)
        {
            if (D) Log.Debug(TAG, "setState() " + mState + " -> " + state);
            mState = state;

            // Give the new state to the Handler so the UI Activity can update
            mHandler.ObtainMessage(SmartLinkBluetoothSerial.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
        }

        /// <summary>
        /// Return the current connection state.
        /// </summary>
        /// <returns>Return the current connection state.</returns>
        public /*synchronized*/ int getState()
        {
            return mState;
        }

        /// <summary>
        /// Start the chat service. Specifically start AcceptThread to begin a
        /// session in listening (server) mode. Called by the Activity onResume()
        /// </summary>
        public /*synchronized*/ void start()
        {
            if (D) Log.Debug(TAG, "start");

            // Cancel any thread attempting to make a connection
            if (mConnectThread != null) { mConnectThread.cancel(); mConnectThread = null; }

            // Cancel any thread currently running a connection
            if (mConnectedThread != null) { mConnectedThread.cancel(); mConnectedThread = null; }

            setState(STATE_NONE);

            //      Listen isn't working with Arduino. Ignore since assuming the phone will initiate the connection.
            //        setState(STATE_LISTEN);
            //
            //        // Start the thread to listen on a BluetoothServerSocket
            //        if (mSecureAcceptThread == null) {
            //            mSecureAcceptThread = new AcceptThread(true);
            //            mSecureAcceptThread.start();
            //        }
            //        if (mInsecureAcceptThread == null) {
            //            mInsecureAcceptThread = new AcceptThread(false);
            //            mInsecureAcceptThread.start();
            //        }
        }

        /// <summary>
        /// Start the ConnectThread to initiate a connection to a remote device.
        /// </summary>
        /// <param name="device">The BluetoothDevice to connect</param>
        /// <param name="secure">Socket Security type - Secure (true) , Insecure (false)</param>
        public /*synchronized*/ void connect(BluetoothDevice device, bool secure)
        {
            if (D) Log.Debug(TAG, "connect to: " + device);

            // Cancel any thread attempting to make a connection
            if (mState == STATE_CONNECTING)
            {
                if (mConnectThread != null) { mConnectThread.cancel(); mConnectThread = null; }
            }

            // Cancel any thread currently running a connection
            if (mConnectedThread != null) { mConnectedThread.cancel(); mConnectedThread = null; }

            // Start the thread to connect with the given device
            mConnectThread = new ConnectThread(device, secure);
            mConnectThread.start();
            setState(STATE_CONNECTING);
        }

        /// <summary>
        /// Start the ConnectedThread to begin managing a Bluetooth connection
        /// </summary>
        /// <param name="socket">The BluetoothSocket on which the connection was made</param>
        /// <param name="device">The BluetoothDevice that has been connected</param>
        /// <param name="String"></param>
        public /*synchronized*/ void connected(BluetoothSocket socket, BluetoothDevice device, string socketType)
        {
            if (D) Log.Debug(TAG, "connected, Socket Type:" + socketType);

            // Cancel the thread that completed the connection
            if (mConnectThread != null) { mConnectThread.cancel(); mConnectThread = null; }

            // Cancel any thread currently running a connection
            if (mConnectedThread != null) { mConnectedThread.cancel(); mConnectedThread = null; }

            // Cancel the accept thread because we only want to connect to one device
            if (mSecureAcceptThread != null)
            {
                mSecureAcceptThread.cancel();
                mSecureAcceptThread = null;
            }
            if (mInsecureAcceptThread != null)
            {
                mInsecureAcceptThread.cancel();
                mInsecureAcceptThread = null;
            }

            // Start the thread to manage the connection and perform transmissions
            mConnectedThread = new ConnectedThread(socket, socketType);
            mConnectedThread.start();

            // Send the name of the connected device back to the UI Activity
            Message msg = mHandler.ObtainMessage(SmartLinkBluetoothSerial.MESSAGE_DEVICE_NAME);
            Bundle bundle = new Bundle();
            bundle.PutString(SmartLinkBluetoothSerial.DEVICE_NAME, device.Name);
            msg.Data = bundle;
            mHandler.SendMessage(msg);

            setState(STATE_CONNECTED);
        }

        /// <summary>
        /// Stop all threads
        /// </summary>
        public /*synchronized*/ void stop()
        {
            if (D) Log.Debug(TAG, "stop");

            if (mConnectThread != null)
            {
                mConnectThread.cancel();
                mConnectThread = null;
            }

            if (mConnectedThread != null)
            {
                mConnectedThread.cancel();
                mConnectedThread = null;
            }

            if (mSecureAcceptThread != null)
            {
                mSecureAcceptThread.cancel();
                mSecureAcceptThread = null;
            }

            if (mInsecureAcceptThread != null)
            {
                mInsecureAcceptThread.cancel();
                mInsecureAcceptThread = null;
            }
            setState(STATE_NONE);
        }

        /// <summary>
        /// Write to the ConnectedThread in an unsynchronized manner
        /// ( see ConnectedThread#write(byte[]) )
        /// </summary>
        /// <param name="output">The bytes to write</param>
        public void write(byte[] output)
        {
            // Create temporary object
            ConnectedThread r;
            // Synchronize a copy of the ConnectedThread
            /*synchronized*/
            (this) {
                if (mState != STATE_CONNECTED) return;
                r = mConnectedThread;
            }
            // Perform the write unsynchronized
            r.write(output);
        }

        /// <summary>
        /// Write to the ConnectedThread in an unsynchronized manner
        /// </summary>
        /// <param name="output">The integers to write</param>
        public void write(int[] output)
        {
            // Create temporary object
            ConnectedThread r;
            // Synchronize a copy of the ConnectedThread
            /*synchronized*/
            (this) {
                if (mState != STATE_CONNECTED) return;
                r = mConnectedThread;
            }
            // Perform the write unsynchronized
            r.write(output);
        }


        /// <summary>
        /// Indicate that the connection attempt failed and notify the UI Activity.
        /// </summary>
        private void connectionFailed()
        {
            // Send a failure message back to the Activity
            mHandler.ObtainMessage(SmartLinkBluetoothSerial.MESSAGE_CONNECTION_FAILED, "Unable to connect to device.").sendToTarget();
            // Start the service over to restart listening mode
            SmartLinkBluetoothSerialService.this.start();
        }


        /// <summary>
        /// Indicate that the connection was lost and notify the UI Activity.
        /// </summary>
        private void connectionLost()
        {
            // Send a failure message back to the Activity
            mHandler.ObtainMessage(SmartLinkBluetoothSerial.MESSAGE_CONNECTION_LOST, "Device connection was lost.").sendToTarget();
            // Start the service over to restart listening mode
            SmartLinkBluetoothSerialService.this.start();
        }

        /// <summary>
        /// This thread runs while listening for incoming connections. It behaves
        /// like a server-side client. It runs until a connection is accepted
        /// (or until cancelled).
        /// </summary>
        private class AcceptThread //extends Thread         // TODO
        {
            // The local server socket
            private readonly BluetoothServerSocket mmServerSocket;
            private string mSocketType;

            public AcceptThread(bool secure)
            {
                BluetoothServerSocket tmp = null;
                mSocketType = secure ? "Secure" : "Insecure";

                // Create a new listening server socket
                try
                {
                    if (secure)
                    {
                        tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, MY_UUID_SECURE);
                    }
                    else
                    {
                        tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE, MY_UUID_INSECURE);
                    }
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
                }
                mmServerSocket = tmp;
            }

            public void run()
            {
                if (D) Log.Debug(TAG, "Socket Type: " + mSocketType + "BEGIN mAcceptThread" + this);
                setName("AcceptThread" + mSocketType);

                BluetoothSocket socket;

                // Listen to the server socket if we're not connected
                while (mState != STATE_CONNECTED)
                {
                    try
                    {
                        // This is a blocking call and will only return on a
                        // successful connection or an exception
                        socket = mmServerSocket.Accept();
                    }
                    catch (IOException e)
                    {
                        Log.Error(TAG, "Socket Type: " + mSocketType + "accept() failed", e);
                        break;
                    }

                    // If a connection was accepted
                    if (socket != null)
                    {
                        /*synchronized*/
                        (SmartLinkBluetoothSerialService.this) {
                            switch (mState)
                            {
                                case STATE_LISTEN:
                                case STATE_CONNECTING:
                                    // Situation normal. Start the connected thread.
                                    connected(socket, socket.RemoteDevice, mSocketType);
                                    break;
                                case STATE_NONE:
                                case STATE_CONNECTED:
                                    // Either not ready or already connected. Terminate new socket.
                                    try
                                    {
                                        socket.Close();
                                    }
                                    catch (IOException e)
                                    {
                                        Log.Error(TAG, "Could not close unwanted socket", e);
                                    }
                                    break;
                            }
                        }
                    }
                }
                if (D) Log.Info(TAG, "END mAcceptThread, socket Type: " + mSocketType);

            }

            public void cancel()
            {
                if (D) Log.Debug(TAG, "Socket Type" + mSocketType + "cancel " + this);
                try
                {
                    mmServerSocket.Close();
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "Socket Type" + mSocketType + "close() of server failed", e);
                }
            }
        }


        /// <summary>
        /// This thread runs while attempting to make an outgoing connection
        /// with a device. It runs straight through; the connection either
        /// succeeds or fails.
        /// </summary>
        private class ConnectThread //: Thread      // TODO
        {
            private readonly BluetoothSocket mmSocket;
            private readonly BluetoothDevice mmDevice;
            private string mSocketType;

            public ConnectThread(BluetoothDevice device, bool secure)
            {
                mmDevice = device;
                BluetoothSocket tmp = null;
                mSocketType = secure ? "Secure" : "Insecure";

                // Get a BluetoothSocket for a connection with the given BluetoothDevice
                try
                {
                    if (secure)
                    {
                        // tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                        tmp = device.CreateRfcommSocketToServiceRecord(UUID_SPP);
                    }
                    else
                    {
                        //tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
                        tmp = device.CreateInsecureRfcommSocketToServiceRecord(UUID_SPP);
                    }
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "Socket Type: " + mSocketType + "create() failed", e);
                }
                mmSocket = tmp;
            }

            public void run()
            {
                Log.Info(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
                setName("ConnectThread" + mSocketType);

                // Always cancel discovery because it will slow down a connection
                mAdapter.cancelDiscovery();

                // Make a connection to the BluetoothSocket
                try
                {
                    // This is a blocking call and will only return on a successful connection or an exception
                    mmSocket.Connect();
                }
                catch (IOException e)
                {
                    Log.Error(TAG, e.ToString());
                    Log.Error(TAG, e.StackTrace);
                    // Close the socket
                    try
                    {
                        mmSocket.Close();
                    }
                    catch (IOException e2)
                    {
                        Log.Error(TAG, "unable to close() " + mSocketType + " socket during connection failure", e2);
                    }
                    connectionFailed();
                    return;
                }

                // Reset the ConnectThread because we're done
                /*synchronized*/(SmartLinkBluetoothSerialService.this) {
                    mConnectThread = null;
                }

                // Start the connected thread
                connected(mmSocket, mmDevice, mSocketType);
            }

            public void cancel()
            {
                try
                {
                    mmSocket.Close();
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "close() of connect " + mSocketType + " socket failed", e);
                }
            }
        }


        /// <summary>
        /// This thread runs during a connection with a remote device.
        /// It handles all incoming and outgoing transmissions.
        /// </summary>
        private class ConnectedThread //extends Thread          // TODO
        {
            private readonly BluetoothSocket mmSocket;
            private readonly InputStream mmInStream;
            private readonly OutputStream mmOutStream;

            public ConnectedThread(BluetoothSocket socket, String socketType)
            {
                Log.Debug(TAG, "create ConnectedThread: " + socketType);
                mmSocket = socket;
                InputStream tmpIn = null;
                OutputStream tmpOut = null;

                // Get the BluetoothSocket input and output streams
                try
                {
                    tmpIn = socket.InputStream;
                    tmpOut = socket.OutputStream;
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "temp sockets not created", e);
                }

                mmInStream = tmpIn;
                mmOutStream = tmpOut;
            }

            public void run()
            {
                Log.Info(TAG, "BEGIN mConnectedThread");
                byte[] buffer = new byte[1024];
                int bytes;
                int len = 0;
                // Keep listening to the InputStream while connected
                while (true)
                {
                    try
                    {
                        //readFromSocket();
                        bytes = mmInStream.read(buffer, len, buffer.length - len);
                        len += bytes;

                        if (len >= 3)
                        {
                            if (buffer[2] != -1)
                            {
                                len = 0;
                                continue;
                            }
                        }

                        if (len >= 5)
                        {
                            int dataLength = Integer.parseInt(String.Format("%02X", buffer[3]) +
                                    String.Format("%02X", buffer[4]), 16);
                            Log.Debug(TAG, "dataLenght: " + dataLength.ToString());

                            if (len == 6 + dataLength)
                            {
                                mHandler.obtainMessage(SmartLinkBluetoothSerial.MESSAGE_READ, len, -1, buffer)
                                        .sendToTarget();
                                len = 0;
                            }
                        }

                    }
                    catch (IOException e)
                    {
                        Log.Error(TAG, "disconnected", e);
                        connectionLost();
                        // Start the service over to restart listening mode
                        SmartLinkBluetoothSerialService.this.start();
                        break;
                    }
                }
            }


            /// <summary>
            /// Write to the connected OutStream.
            /// </summary>
            /// <param name="buffer">The bytes to write</param>
            public void write(byte[] buffer)
            {
                try
                {
                    mmOutStream.write(buffer);

                    // Share the sent message back to the UI Activity
                    mHandler.obtainMessage(SmartLinkBluetoothSerial.MESSAGE_WRITE, -1, -1, buffer).sendToTarget();

                }
                catch (IOException e)
                {
                    Log.Error(TAG, "Exception during write", e);
                }
            }

            public void write(int[] buffer)
            {
                try
                {
                    for (int i = 0; i < buffer.length; i++)
                    {
                        mmOutStream.write(buffer[i]);
                    }
                    // Share the sent message back to the UI Activity
                    mHandler.obtainMessage(SmartLinkBluetoothSerial.MESSAGE_WRITE, -1, -1, buffer)
                            .sendToTarget();
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "Exception during write", e);
                }

            }


            public void cancel()
            {
                try
                {
                    mmSocket.Close();
                }
                catch (IOException e)
                {
                    Log.Error(TAG, "close() of connect socket failed", e);
                }
            }
        }
    }
}