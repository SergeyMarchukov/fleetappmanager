using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    public enum SmartLinkNegativeResponse
    {
        generalReject(0x10),
        serviceNotSupported(0x11),
        subFunctionNotSupported(0x12),
        incorrectMessageLengthOrInvalidFormat(0x13),
        busyRepeatRequest(0x21),
        conditionsNotCorrect(0x22),
        requestSequenceError(0x24),
        requestOutOfRange(0x31),
        securityAccessDenied(0x33),
        invalidKey(0x35),
        exceededNumberOfAttempts(0x36),
        generalProgrammingFailure(0x72),
        requestCorrectlyReceivedResponsePending(0x78),
        serviceNotSupportedInActiveSession(0x7F),
        TLVObjectsAreNotConsistent(0xFA)
    ;

    private int enumId;

    SmartLinkNegativeResponse(int i)
    {
        enumId = i;
    }

    int value()
    {
        return enumId;
    }

    public static SmartLinkNegativeResponse get(int i)
    {
        for (SmartLinkNegativeResponse r: SmartLinkNegativeResponse.values())
        {
            if (r.value() == i)
            {
                return r;
            }
        }

        return null;
    }

    public String toMessage()
    {
        return String.format("negative response code %s(%02X)", toString(), enumId);
    }
}
}