using Android.Content;
using Android.OS;
using Android.Util;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 10.03.17
    /// </summary>
    public class SmartLinkLogger
    {
        private readonly static SmartLinkLogger _instance = new SmartLinkLogger();

        private static String logFileName = "smartlink.log";
        private static List<string> _buffer;
        private static int maxBufferSize = 250;
        private Handler handler;

        private SmartLinkLogger()
        {
            _buffer = new List<string>();
        }

        private void sendMessageToTarget()
        {
            if (handler != null && _buffer.Count > 0)
            {
                handler.ObtainMessage(SmartLink.MESSAGE_NEW_LOG_ENTRY, _buffer[_buffer.Count - 1]).sendToTarget();
            }
        }


        public static SmartLinkLogger getLogger()
        {
            return _instance;
        }

        //@Override
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _buffer.Count; i++)
            {
                sb.Append(sb.Append(_buffer[i]));
            }
            return sb.ToString();
        }

        public void writeStartCommand(SmartLinkCmd cmd)
        {
            string msg = string.Format("Execute command: %s on interface %d", cmd.getCommandName(), cmd.getInterface());
            writeMessage(msg);
        }

        public void setHandler(Handler h)
        {
            handler = h;
        }

        public void writeCommandResult(SmartLinkOperationResult result)
        {
            StringBuilder sb = new StringBuilder();
            if (result.cmd != null)
            {
                sb.Append(string.Format("Result for command: %s \r\n", result.cmd.getCommandName()));
            }
            if (result.is_ok())
            {
                sb.Append("Operation success success.\r\n");
                if (result.getData() instanceof JSONObject) {
                    sb.Append(result.getData().ToString() + "\n");
                }

            }
            else
            {
                sb.Append(result.message + "\r\n");
            }
            if (result.cmd != null)
            {
                if (result.cmd.getlastSmartLinkMessage() != null)
                {
                    sb.Append(string.Format("Raw data: %s\n", result.cmd.getlastSmartLinkMessage().ToString()));
                }
            }
            writeMessage(sb.ToString());
        }



        public /*synchronized*/ void writeMessage(string message)
        {
            StringBuilder sb = new StringBuilder();
            //SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
            DateTime dt = new DateTime();
            sb.Append('[');
            sb.Append(dt.ToString("yyyy.MM.dd HH:mm:ss z"));
            sb.Append("] ");
            sb.Append(message);
            sb.Append("\r\n");

            if (_buffer.Count > maxBufferSize)
            {
                _buffer.Remove(0);
            }
            _buffer.Add(sb.ToString());
            sendMessageToTarget();
        }

        public void SaveToFile(Context context)
        {
            SaveToFile(context, logFileName);
        }

        public /*synchronized*/ void SaveToFile(Context context, string filename)
        {
            try
            {
                FileOutputStream output = context.OpenFileOutput(filename, Context.MODE_MULTI_PROCESS);
                output.write(toString().getBytes());
                output.flush();
                output.close();
            }
            catch (FileNotFoundException e)
            {
                Log.Error("", e.StackTrace);
            }
            catch (IOException e)
            {
                Log.Error("", e.StackTrace);
            }
        }
    }
}