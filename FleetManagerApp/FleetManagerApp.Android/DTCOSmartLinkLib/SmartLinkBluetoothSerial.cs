using Android.Bluetooth;
using Android.OS;
using Android.Util;
using FleetManagerApp.Droid.DTCOSmartLinkLib;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 10.03.17
    /// </summary>
    public class SmartLinkBluetoothSerial
    {
        private static readonly SmartLinkBluetoothSerial sharedInstance = new SmartLinkBluetoothSerial();
        private bool _firmwareUpgradeStart;
        private int mLastIface;

        public static SmartLinkBluetoothSerial getInstance()
        {
            return sharedInstance;
        }

        private static ISmartLinkOperationCallback smartLinkOperationCallback = null;
        private static ISmartLinkOperationCallback connectionCallback = null;

        private static SmartLinkStateCallbacksList _statesCallbacks;

        private bool _IsSendingPin = false;

        protected BluetoothAdapter bluetoothAdapter;
        protected SmartLinkBluetoothSerialService bluetoothSerialService;

        // Debugging
        private static readonly String TAG = "SmartLinkBluetoothSerial";
        private static readonly bool D = true;

        public static readonly int PIN_LENGTH = 6;

        // Message types sent from the BluetoothSerialService Handler
        public static readonly int MESSAGE_STATE_CHANGE = 1;
        public static readonly int MESSAGE_READ = 2;
        public static readonly int MESSAGE_WRITE = 3;
        public static readonly int MESSAGE_DEVICE_NAME = 4;
        public static readonly int MESSAGE_CONNECTION_FAILED = 5;
        public static readonly int MESSAGE_CONNECTION_LOST = 6;
        protected static readonly int MESSAGE_SEND_ERROR_CALLBACK = 20001;
        protected static readonly int MESSAGE_SEND_INTERRUPT_CALLBACK = 20011;
        protected static readonly int MESSAGE_SEND_CALLBACK = 20002;

        // Key names received from the BluetoothChatService Handler
        public static readonly string DEVICE_NAME = "device_name";

        private static int mSessionToken = 255;
        private String mLastCommand = null;

        private String _Pin = "";
        private int mMaxCommandWaitTries = 10;
        private int mHeartBeatInterval = 30000;
        private SmartLinkMessage _lastSmartLinkMessage = null;
        private CommandExecuteThread mCommandThread;
        private SmartLinkWatchDogThread smartLinkWatchDogThread;
        private readonly static LinkedList<SmartLinkCmd> _commandQueue = new LinkedList<SmartLinkCmd>();

        private SmartLinkCmd foundWaitingCommand(SmartLinkMessage msg)
        {
            foreach (SmartLinkCmd cmd in _commandQueue)
            {
                if (cmd.isMyResponse(msg))
                {
                    return cmd;
                }
            }
            return null;
        }


        public void addStateCallback(ISmartLinkStateCallback smartLinkStateCallback)
        {
            if (smartLinkStateCallback != null)
            {
                _statesCallbacks.Add(smartLinkStateCallback);
            }
        }

        private SmartLinkBluetoothSerial()
        {
            if (bluetoothAdapter == null)
            {
                bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            }
            initBluetoothSerialService();
            _statesCallbacks = new SmartLinkStateCallbacksList();
            mCommandThread = new CommandExecuteThread();
            mCommandThread.start();
            smartLinkWatchDogThread = new SmartLinkWatchDogThread();
            smartLinkWatchDogThread.start();

        }

        private void restartCommandThread()
        {
            if ((mCommandThread == null) || mCommandThread.isInterrupted())
            {
                mCommandThread = null;
                mCommandThread = new CommandExecuteThread();
                mCommandThread.start();
            }
        }

        private void initBluetoothSerialService()
        {
            if (bluetoothSerialService == null)
            {
                bluetoothSerialService = new SmartLinkBluetoothSerialService(mHandler);
            }
        }

        public void onDestroy()
        {
            //super.onDestroy();
            if (bluetoothSerialService != null)
            {
                closeSession();
                bluetoothSerialService.stop();
            }
        }

        internal bool isConnected()
        {
            return bluetoothSerialService.getState() == SmartLinkBluetoothSerialService.STATE_CONNECTED;
        }

        internal bool isConnecting()
        {
            return bluetoothSerialService.getState() == SmartLinkBluetoothSerialService.STATE_CONNECTING;
        }

        public bool isEnabled()
        {
            return bluetoothAdapter.IsEnabled;
        }

        internal bool isSessionOpen()
        {
            bool r;
            r = mSessionToken < 255 && mSessionToken > 0;
            if (!r)
            {
                mSessionToken = 255;
                return r;
            }

            return r;
        }

        internal bool isSmartLinkConnected()
        {
            return (isConnected() && isSessionOpen());
        }

        protected void write(byte[] data)
        {
            bluetoothSerialService.write(data);
        }

        internal void disconnect()
        {
            closeSession();
            bluetoothSerialService.stop();
        }

        private List<BluetoothDevice> _listBondedDevices()
        {
            List<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
            return bondedDevices;
        }

        internal List<SmartLinkDeviceData> listBondedDevices()
        {
            List<SmartLinkDeviceData> result = new List<SmartLinkDeviceData>();
            foreach (BluetoothDevice d in _listBondedDevices())
            {
                if (isVDODevice(d))
                {
                    result.Add(new SmartLinkDeviceData(d));
                }
            }
            return result;
        }

        protected void connect(String macAddress, bool secure, ISmartLinkOperationCallback callback)
        {
            SmartLinkOperationResult result = new SmartLinkOperationResult();
            if (!isEnabled())
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_NOT_ENABLE));
                return;
            }
            BluetoothDevice device;
            try
            {
                device = bluetoothAdapter.GetRemoteDevice(macAddress);
            }
            catch (ArgumentException e)//(IllegalArgumentException e)
            {
                callback.onSmartLinkOperationResult(result.setError(e.Message));
                return;
            }

            if (device == null)
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_DEVICE_NOT_FOUND));
                return;
            }
            if (!isVDODevice(device))
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_DEVICE_NOT_SMARTLINK));
                return;
            }
            if (!isConnected())
            {
                //if (!isConnecting()) {
                connectionCallback = callback;
                bluetoothSerialService.connect(device, secure);
                return;
                //}
            }

            callback.onSmartLinkOperationResult(result);
        }


        protected void connect(String macAddress, string pin, ISmartLinkOperationCallback callback)     // TODO: overrinding
        {

            //connect(macAddress, false, new SmartLinkOperationCallback()
            //{
            //    //@Override
            //    public void onSmartLinkOperationResult(SmartLinkOperationResult result)
            //    {
            //        if (result.is_ok())
            //        {
            //            Log.Debug(TAG, "Connection ok. sending pin");
            //            sendPin(pin, callback);
            //        }
            //        else
            //        {
            //            Log.Debug(TAG, "Connection fail.");
            //            callback.onSmartLinkOperationResult(result);
            //        }
            //    }
            //});
        }


        protected void sendPin(String pin, ISmartLinkOperationCallback callback)
        {
            SmartLinkOperationResult result = new SmartLinkOperationResult();

            if (!isEnabled())
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_NOT_ENABLE));
                return;
            }

            if (!isConnected())
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_NOT_CONNECTED));
                return;
            }

            if (isSessionOpen())
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.SESSION_ALREADY_OPEN));
                return;
            }

            if (pin == null || pin.isEmpty())
            {
                pin = _Pin;
            }

            if (pin.length() != PIN_LENGTH)
            {
                callback.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.PIN_TOO_SHORT));
                return;
            }


            if (smartLinkOperationCallback != null)
            {
                try
                {
                    smartLinkOperationCallback.onSmartLinkOperationResult(result.setError("Staled callback."));
                }
                catch (Exception e)
                {

                }
            }
            _Pin = pin;

            _IsSendingPin = true;
            _sendMessageToSmartLink(SmartLinkConstants.IFACE_SESSION_HANDLING, "00" + checkAndConvertPassword(pin));
            smartLinkOperationCallback = callback;


        }

        internal void closeSession()
        {
            if (mSessionToken != 255)
            {
                _sendMessageToSmartLink(SmartLinkConstants.IFACE_SESSION_HANDLING, "01");
                mSessionToken = 255;
                _firmwareUpgradeStart = false;
            }
        }

        internal void forceCloseSession(ISmartLinkOperationCallback callback)
        {
            forceCloseSession(null, callback);
        }

        internal void forceCloseSession(String pin, ISmartLinkOperationCallback callback)
        {
            if (pin == null || pin.isEmpty())
            {
                pin = _Pin;
            }
            if (pin.isEmpty())
            {
                return;
            }
            killQueue();
            _sendMessageToSmartLink(SmartLinkConstants.IFACE_SESSION_HANDLING, "02" + checkAndConvertPassword(pin));
            smartLinkOperationCallback = callback;

        }


        internal bool isVDODevice(BluetoothDevice device)
        {
            String strDeviceName = device.Name.Substring(0, 3);
            return (strDeviceName.equalsIgnoreCase("VDO"));
        }

        //// The Handler that gets information back from the BluetoothSerialService
        //// Original code used handler for the because it was talking to the UI.
        //// Consider replacing with normal callbacks
        //private Handler mHandler = new Handler()        // TODO:
        //{

        //    public void handleMessage(Message msg)
        //{
        //    switch (msg.What)
        //    {
        //        case MESSAGE_READ:
        //            notifyDataRead((byte[])msg.obj, msg.arg1);
        //            break;
        //        case MESSAGE_STATE_CHANGE:

        //            if (D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
        //            switch (msg.arg1)
        //            {
        //                case SmartLinkBluetoothSerialService.STATE_CONNECTED:
        //                    Log.i(TAG, "BluetoothSerialService.STATE_CONNECTED");
        //                    notifyConnectionSuccess();
        //                    break;
        //                case SmartLinkBluetoothSerialService.STATE_CONNECTING:
        //                    Log.i(TAG, "BluetoothSerialService.STATE_CONNECTING");
        //                    break;
        //                case SmartLinkBluetoothSerialService.STATE_LISTEN:
        //                    Log.i(TAG, "BluetoothSerialService.STATE_LISTEN");
        //                    break;
        //                case SmartLinkBluetoothSerialService.STATE_NONE:
        //                    Log.i(TAG, "BluetoothSerialService.STATE_NONE");
        //                    notifyConnectionFailed("");
        //                    break;
        //            }
        //            break;
        //        case MESSAGE_WRITE:
        //            //  byte[] writeBuf = (byte[]) msg.obj;
        //            //  String writeMessage = new String(writeBuf);
        //            //  Log.i(TAG, "Wrote: " + writeMessage);
        //            break;
        //        case MESSAGE_DEVICE_NAME:
        //            Log.i(TAG, msg.getData().getString(DEVICE_NAME));
        //            break;
        //        case MESSAGE_CONNECTION_LOST:
        //            notifyConnectionLost((String)msg.obj);
        //            break;
        //        case MESSAGE_CONNECTION_FAILED:
        //            notifyConnectionFailed((String)msg.obj);
        //            break;
        //        case MESSAGE_SEND_ERROR_CALLBACK:
        //            sendSmartLinkCommandErrorResult((SmartLinkErrorCode)msg.obj, false);
        //            break;
        //        case MESSAGE_SEND_INTERRUPT_CALLBACK:
        //            if (msg.obj instanceof SmartLinkOperationCallback) {
        //                SmartLinkOperationCallback c = (SmartLinkOperationCallback)msg.obj;
        //                c.onSmartLinkOperationResult(SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED));
        //            }
        //            break;
        //    }
        //}
        //}

        /// <summary>
        /// Set the smart link heart beet interval
        /// </summary>
        /// <param name="msec"></param>
        internal void setHeartBeatInterval(int msec)
        {
            mHeartBeatInterval = msec;
        }

        private sealed class SmartLinkWatchDogThread //extends Thread implements SmartLinkOperationCallback
        {
            //private final SmartLinkCmdTesterPresent command;
            private readonly SmartLinkCmdTimeDate command;

            private SmartLinkOperationResult _result;

            public SmartLinkWatchDogThread()
            {
                //TODO need to replace another command
                command = new SmartLinkCmdTimeDate(this);
                _result = null;
            }

            public void run()
            {
                while (!isInterrupted())
                {
                    try
                    {
                        Thread.Sleep(mHeartBeatInterval);
                        if (isSmartLinkConnected())
                        {
                            while ((smartLinkOperationCallback != null) || (!_commandQueue.isEmpty()))
                            {
                                int k = _commandQueue.size() > 0 ? _commandQueue.size() : 1;
                                Thread.Sleep(500 * k);
                            }
                            _result = null;
                            command.execute();
                            while (_result == null)
                            {
                                Thread.Sleep(500);
                            }
                        }
                    }
                    catch (ThreadInterruptedException e)
                    {

                    }

                }
            }


            public void onSmartLinkOperationResult(SmartLinkOperationResult result)
            {
                _result = result;
                if (!result.is_ok())
                {
                    //TODO need to check session error end reconnect
                }

            }
        }

        private class CommandExecuteThread //extends Thread         // TODO
        {

            private SmartLinkCmd _currentCommand = null;

            public SmartLinkCmd getCurrentCommand()
            {
                return _currentCommand;
            }

            public bool _isFreeze;

            public CommandExecuteThread()
            {
                _isFreeze = false;
            }

            public void run()
            {
                try
                {
                    while (!isInterrupted())
                    {
                        int try_num = 1;
                        while (smartLinkOperationCallback != null)
                        {
                            Thread.Sleep(500);
                            while (_isFreeze)
                            {
                                Thread.Sleep(500);
                            }
                            if (try_num >= mMaxCommandWaitTries)
                            {
                                mHandler.obtainMessage(SmartLinkBluetoothSerial.MESSAGE_SEND_ERROR_CALLBACK, SmartLinkErrorCode.COMMAND_INTERRUPTED).sendToTarget();
                                break;
                                //sendSmartLinkCommandErrorResult(SmartLinkErrorCode.COMMAND_INTERRUPTED, false);
                            }
                            try_num++;
                        }
                        while (_isFreeze)
                        {
                            Thread.sleep(500);
                        }
                        if (isInterrupted() && _currentCommand != null)
                        {
                            mHandler.obtainMessage(SmartLinkBluetoothSerial.MESSAGE_SEND_INTERRUPT_CALLBACK, SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED)).sendToTarget();
                            //_currentCommand.onSmartLinkOperationResult(SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED));
                            break;
                        }
                        if (!_commandQueue.isEmpty())
                        {
                            _currentCommand = _commandQueue.pop();
                            _sendMessageToSmartLink(_currentCommand.getInterface(), _currentCommand.getCommandMessage());
                            smartLinkOperationCallback = _currentCommand;
                        }
                    }

                }
                catch (ThreadInterruptedException e)
                {

                }
            }

            public void freeze()
            {
                _isFreeze = true;
            }

            public void unfreeze()
            {
                _isFreeze = false;
            }

        }


        protected void notifyConnectionLost(String error)
        {
            closeSession();
            _statesCallbacks.onSmartLinkDisconnected(SmartLinkOperationResult.FactoryError(error, SmartLinkErrorCode.BLUETOOTH_CONNECTION_LOST));
            clearCommandQueue(SmartLinkOperationResult.FactoryError(error, SmartLinkErrorCode.BLUETOOTH_CONNECTION_LOST));
        }

        protected void notifyConnectionFailed(String error)
        {
            if (connectionCallback != null)
            {
                connectionCallback.onSmartLinkOperationResult(SmartLinkOperationResult.FactoryError(error, SmartLinkErrorCode.BLUETOOTH_CONNECTION_FAILED));
                connectionCallback = null;
            }
            closeSession();
            clearCommandQueue(SmartLinkOperationResult.FactoryError(error, SmartLinkErrorCode.BLUETOOTH_CONNECTION_FAILED));
            _statesCallbacks.onSmartLinkDisconnected(SmartLinkOperationResult.FactoryError(error, SmartLinkErrorCode.BLUETOOTH_CONNECTION_FAILED));
        }

        protected void notifyConnectionSuccess()
        {
            if (connectionCallback != null)
            {
                connectionCallback.onSmartLinkOperationResult(SmartLinkOperationResult.Factory());
                connectionCallback = null;
            }
        }

        protected void sendSmartLinkCommandErrorResult(SmartLinkErrorCode code, bool cleanUpQueue)
        {
            sendSmartLinkCommandErrorResult(SmartLinkOperationResult.FactoryError(code), cleanUpQueue);
        }

        protected void sendSmartLinkCommandErrorResult(String message, SmartLinkErrorCode code, bool cleanUpQueue)
        {
            sendSmartLinkCommandErrorResult(SmartLinkOperationResult.FactoryError(message, code), cleanUpQueue);
        }

        protected void sendSmartLinkCommandErrorResult(SmartLinkOperationResult r, bool cleanUpQueue)
        {
            if (smartLinkOperationCallback != null)
            {
                smartLinkOperationCallback.onSmartLinkOperationResult(r);
                smartLinkOperationCallback = null;
            }
            else
            {
                _statesCallbacks.onSmartLinkUnProcessedResponse(r);
            }
            if (cleanUpQueue)
            {
                clearCommandQueue(r);
            }
        }

        protected void sendSmartLinkCommandResult(Object data)
        {
            sendSmartLinkCommandResult(SmartLinkOperationResult.Factory(data), false);
        }

        protected void sendSmartLinkCommandResult(SmartLinkOperationResult r, bool cleanUpQueue)
        {
            if (smartLinkOperationCallback != null)
            {
                smartLinkOperationCallback.onSmartLinkOperationResult(r);
                smartLinkOperationCallback = null;
            }
            else
            {
                _statesCallbacks.onSmartLinkUnProcessedResponse(r);
            }

            if (cleanUpQueue)
            {
                clearCommandQueue(SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED));
            }

        }

        protected void notifyDataRead(byte[] answer, int len)
        {
            if (len >= 5)
            {

                int dataLength = Integer.parseInt(String.Format("%02X", answer[3]) +
                        String.Format("%02X", answer[4]), 16);
                Log.Debug(TAG, "dataLenght: " + dataLength.ToString());

                if (len == 6 + dataLength)
                {
                    SmartLinkMessage smartLinkMessage = new SmartLinkMessage(answer);
                    Log.Debug(TAG, smartLinkMessage.toString());
                    int iface = smartLinkMessage.getIface();


                    byte[] temp = new byte[len - 1];
                    for (int i = 0; i < len - 1; i++)
                    {
                        temp[i] = answer[i];
                    }
                    _lastSmartLinkMessage = smartLinkMessage;
                    if (!smartLinkMessage.getHexCheckSum().equals(getChecksum(temp)))
                    {
                        Log.Debug("SmartLink", "Received Data with wrong checksum!");
                        sendSmartLinkCommandErrorResult(SmartLinkErrorCode.COMMAND_REPLY_WRONG_CHECKSUM, false);
                        return;
                    }
                    if (isValidForSendMessageInterface(iface))
                    {
                        onMessageReply(smartLinkMessage);
                    }
                    else if (iface == SmartLinkConstants.IFACE_SESSION_HANDLING)
                    {
                        handleSessionReply(smartLinkMessage);
                    }
                    else if (iface == SmartLinkConstants.IFACE_ERROR)
                    {
                        handleErrorReply(smartLinkMessage);
                    }
                }
            }
        }

        private void handleErrorReply(SmartLinkMessage smartLinkMessage)
        {
            try
            {

                String data = smartLinkMessage.getData();
                int error_code = Integer.parseInt(data.Substring(2, 4), 16);
                int iface = Integer.parseInt(data.Substring(0, 2));


                switch (error_code)
                {
                    case ERROR_RESPONSE_BUSY:
                        Log.d(TAG, String.format("Interface busy repeating last command %s", mLastCommand));
                        _sendMessageToSmartLink(iface, mLastCommand);
                    case ERROR_RESPONSE_WRONG_PIN:
                        mSessionToken = 255;
                        sendSmartLinkCommandErrorResult(SmartLinkErrorCode.INVALID_PIN, false);
                        return;
                    case ERROR_RESPONSE_TOKEN_ERROR:
                        //reCreateSessionAndRepeatCommand();
                        sendSmartLinkCommandErrorResult(SmartLinkErrorCode.TOKEN_ERROR, false);
                        return;
                    case ERROR_RESPONSE_INVALID_TOKEN:
                        //reCreateSessionAndRepeatCommand();
                        sendSmartLinkCommandErrorResult(SmartLinkErrorCode.SESSION_DIFFERENT_TOKENS, true);
                        return;
                    case ERROR_RESPONSE_OVER_SESSION_ACTIVE:
                        //reCreateSessionAndRepeatCommand();
                        sendSmartLinkCommandErrorResult(SmartLinkErrorCode.SESSION_ALREADY_OPEN, true);
                        return;
                    case ERROR_RESPONSE_SL_UPDATE_MODE:
                        ///_firmwareUpgradeStart = true;
                        sendSmartLinkCommandErrorResult(SmartLinkErrorCode.FIRWARE_UPGRADE_IN_PROCESS, false);
                        return;
                    default:
                        sendSmartLinkCommandErrorResult(SmartLinkOperationResult.Factory(smartLinkMessage).setError(SmartLinkErrorCode.GENERAL_ERROR), false);
                        break;
                }


            }
            catch (NumberFormatException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                Log.Error(e.StackTrace);
            }
        }

        private void handleSessionReply(SmartLinkMessage smartLinkMessage)
        {
            String data = smartLinkMessage.getData();
            if (data.Equals("00"))
            {
                mSessionToken = smartLinkMessage.getSessionToken();
                _statesCallbacks.onSmartLinkConnected(new SmartLinkOperationResult());
                sendSmartLinkCommandResult(new SmartLinkOperationResult());
                _IsSendingPin = false;
                return;
            }
            if (data.equals("01"))
            {
                mSessionToken = 255;
                sendSmartLinkCommandResult(new SmartLinkOperationResult());
                return;
            }
            if (data.equals("02"))
            {
                mSessionToken = 255;
                sendSmartLinkCommandResult(new SmartLinkOperationResult());
                return;
            }
        }

        private void onMessageReply(SmartLinkMessage smartLinkMessage)
        {
            try
            {

                SmartLinkOperationResult result = SmartLinkOperationResult.Factory(smartLinkMessage);
                sendSmartLinkCommandResult(result, false);

            }
            catch (Exception e)
            {
                sendSmartLinkCommandResult(SmartLinkOperationResult.FactoryError(e.Message));
            }
        }

        protected void clearCommandQueue(SmartLinkOperationResult r)
        {
            if (smartLinkOperationCallback != null)
            {
                smartLinkOperationCallback.onSmartLinkOperationResult(SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED));
                smartLinkOperationCallback = null;
            }
            if (connectionCallback != null)
            {
                connectionCallback.onSmartLinkOperationResult(SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED));
            }

            mCommandThread = null;
            foreach (SmartLinkCmd cmd in _commandQueue)
            {
                cmd.onSmartLinkOperationResult(r);
                _commandQueue.Clear();
            }
            restartCommandThread();
        }

        protected void killQueue()
        {
            mCommandThread = null;
            _commandQueue.clear();
            restartCommandThread();
        }

        public void processFirmwareUpgrade(Byte[] data, ISmartLinkOperationCallback callback)
        {
            _firmwareUpgradeStart = true;
            clearCommandQueue(SmartLinkOperationResult.FactoryError(SmartLinkErrorCode.COMMAND_INTERRUPTED));

        }

        protected void _startFirmwareUpgrade(Byte[] data)
        {

        }

        protected void _continueFirmwareUpgrade(Byte[] data)
        {

        }

        protected void _endFirmwareUpgrade()
        {
            _firmwareUpgradeStart = false;
        }


        public void executeCommand(SmartLinkCmd cmd)
        {
            SmartLinkOperationResult result = new SmartLinkOperationResult();
            if (!isEnabled())
            {
                cmd.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_NOT_ENABLE));
                return;
            }
            if (!isConnected())
            {
                cmd.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.BLUETOOTH_NOT_CONNECTED));
                return;
            }
            if (!isSessionOpen())
            {
                cmd.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.SESSION_NOT_OPEN));
                return;
            }

            if (!isValidForSendMessageInterface(cmd.getInterface()))
            {
                cmd.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.INVALID_INTERFACE_FOR_COMMAND));
                return;
            }
            if (_firmwareUpgradeStart && (cmd.getInterface() != SmartLinkConstants.IFACE_UPGRADE))
            {
                cmd.onSmartLinkOperationResult(result.setError(SmartLinkErrorCode.FIRWARE_UPGRADE_IN_PROCESS));
                return;
            }


            _commandQueue.add(cmd);
            restartCommandThread();

        }

        private void _sendMessageToSmartLink(int Interface, String data)
        {
            if (data.Length > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(String.Format("%02X", Interface));
                sb.Append(String.Format("%02X", mSessionToken));
                sb.Append(String.Format("%02X", 0xFF));
                sb.Append(String.Format("%04X", data.Length / 2));
                sb.Append(data);
                sb.Append(SmartLinkHelpers.getChecksum(SmartLinkHelpers.hexStringToByteArray(sb.ToString())));
                mLastCommand = data;
                mLastIface = Interface;

                Log.Debug(TAG, String.Format("Sending command %s to interface %d", mLastCommand, mLastIface));
                int[] send = SmartLinkHelpers.hexStringToIntArray(sb.ToString());

                bluetoothSerialService.write(send);


            }
        }
    }
}