using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Created by Sergey M. 19.03.17
/// </summary>
namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    public class SmartLinkPIN
    {
        public int IntValue { get; private set; }

        public List<byte> ByteValue { get; private set; }

        public SmartLinkPIN(string pin)
        {
            string pattern = @"\d+";
            Regex reg = new Regex(pattern);

            if (reg.IsMatch(pin))
            {
                IntValue = Convert.ToInt32(pin);

                ByteValue.Add(0x00);
                ByteValue.AddRange(Encoding.UTF8.GetBytes(pin));
            }
        }
    }
}