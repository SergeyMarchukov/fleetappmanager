using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 10.03.17
    /// </summary>
    public interface ISmartLinkStateCallback
    {
        void onSmartLinkDisconnected(SmartLinkOperationResult result);

        void onSmartLinkConnected(SmartLinkOperationResult result);

        void onSmartLinkUnProcessedResponse(SmartLinkOperationResult result);
    }
}