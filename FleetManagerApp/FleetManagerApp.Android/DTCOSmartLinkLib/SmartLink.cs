using Android.Bluetooth;
using Android.Content;
using Android.OS;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Commands;
using System;
using System.Collections.Generic;
using System.Threading;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 13.03.17
    /// </summary>
    public class SmartLink : ISmartLinkOperationCallback, ISmartLinkStateCallback
    {
        public static readonly int MESSAGE_RESULT_GET = 1;
        public static readonly int MESSAGE_CONNECTED = 2;
        public static readonly int MESSAGE_DISCONNECTED = 3;
        public static readonly int MESSAGE_UNPROCESSED_RESULT_GET = 4;
        public static readonly int MESSAGE_PIN_OK = 1001;
        public static readonly int MESSAGE_PIN_FAIL = 1002;
        public static readonly int MESSAGE_NEW_LOG_ENTRY = 2001;


        private SmartLinkBluetoothSerial SL;
        private Handler handler;
        private Intent intent;
        private SmartLinkLogger smartLinkLogger;

        public SmartLink()
        {
            _init(null, null);
        }

        public SmartLink(Handler h)
        {
            _init(h, null);
        }

        private SmartLink(Handler h, Intent i)
        {
            _init(h, i);
        }

        private void _init(Handler h, Intent i)
        {
            SL = SmartLinkBluetoothSerial.getInstance();
            smartLinkLogger = SmartLinkLogger.getLogger();
            SL.addStateCallback(this);
            handler = h;
            smartLinkLogger.setHandler(h);
            intent = i;
        }

        public void setHandler(Handler h)
        {
            handler = h;
        }

        public void setIntent(Intent i)
        {
            intent = i;
        }

        public bool isConnected()
        {
            return SL.isConnected();
        }

        public bool isConnecting()
        {
            return SL.isConnecting();
        }

        public bool isBlueToothEnabled()
        {
            return SL.isEnabled();
        }

        public bool isSessionOpen()
        {
            return SL.isSessionOpen();
        }

        public bool isSmartLinkConnected()
        {
            return SL.isSmartLinkConnected();
        }

        public void disconnect()
        {
            SL.disconnect();
        }

        public List<SmartLinkDeviceData> listBondedDevices()
        {
            return SL.listBondedDevices();
        }

        public void closeSession()
        {
            SL.closeSession();
        }

        public void forceCloseSession(String pin)
        {
            smartLinkLogger.writeMessage(String.Format("Trying reset session"));
            //SL.forceCloseSession(pin, new SmartLinkOperationCallback() {
            ////@Override
            //    public void onSmartLinkOperationResult(SmartLinkOperationResult result)
            //    {
            //        if (result.is_ok())
            //        {
            //            sendMessageToTarget(MESSAGE_RESULT_GET, result);
            //            smartLinkLogger.writeMessage("Reset ok. Try send pin now");
            //        }
            //        else
            //        {
            //            sendMessageToTarget(MESSAGE_RESULT_GET, result);
            //            smartLinkLogger.writeCommandResult(result);
            //        }
            //    }
            //});
        }

        public bool isVDODevice(BluetoothDevice device)
        {
            return SL.isVDODevice(device);
        }

        public void setHeartBeatInterval(int msec)
        {
            SL.setHeartBeatInterval(msec);
        }


        protected bool connectSync(String macAddress, String pin)
        {
            SmartLinkOperationResult[] _result = { null };
            //SL.connect(macAddress, pin, new SmartLinkOperationCallback() {
            //    //@Override
            //    public void onSmartLinkOperationResult(SmartLinkOperationResult result)
            //    {
            //        _result[0] = result;
            //    }
            //});

            while (_result[0] == null)
            {
                try
                {
                    Thread.Sleep(500);
                }
                catch (ThreadInterruptedException e)//(InterruptedException e)
                {
                    return false;
                }
            }

            if (_result[0] == null)
            {
                return false;
            }

            if (_result[0].is_ok())
            {
                return true;
            }

            return false;
        }

        protected void sendMessageToTarget(int what, SmartLinkOperationResult result)
        {
            if (handler != null)
            {
                handler.ObtainMessage(what, result).sendToTarget();
            }
        }


        public void connect(String macAddress, String pin)
        {
            smartLinkLogger.writeMessage(String.Format("Connect to device %s with pin %s", macAddress, pin));
            //SL.connect(macAddress, pin, new SmartLinkOperationCallback() {
            //    //@Override
            //    public void onSmartLinkOperationResult(SmartLinkOperationResult result)
            //    {
            //        if (result.is_ok())
            //        {
            //            sendMessageToTarget(MESSAGE_PIN_OK, result);
            //            smartLinkLogger.writeMessage("Pin ok");
            //        }
            //        else
            //        {
            //            sendMessageToTarget(MESSAGE_PIN_FAIL, result);
            //            smartLinkLogger.writeCommandResult(result);
            //        }
            //    }
            //});
    }

    public SmartLinkCmdRDBI createCommand(SmartLinkCmdRDBI.SmartLinkCmdRDBIDictionary c)
    {
        try
        {
            SmartLinkCmdRDBI cmd = SmartLinkCmdRDBI.Factory(c, this);
            cmd.setSmartLinkLogger(smartLinkLogger);
            return cmd;
        }
        catch (SmartLinkCommandClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public SmartLinkCmdConfiguration createCommand(SmartLinkCmdConfiguration.ConfigurationPart c)
    {
        SmartLinkCmdConfiguration cmd = SmartLinkCmdConfiguration.Factory(c, this);
        cmd.setSmartLinkLogger(smartLinkLogger);
        return cmd;
    }

    public void processAllConfigurationCommands()
    {
        SmartLinkCmdConfiguration.processAll(this, smartLinkLogger);

    }

    public SmartLinkCmd createCommand(int iface, String commandMessage)
    {
        SmartLinkCmd cmd = SmartLinkCmd.Factory(iface, commandMessage, this);
        cmd.setSmartLinkLogger(smartLinkLogger);
        return cmd;
    }
        
    public void onSmartLinkOperationResult(SmartLinkOperationResult result)
    {
        sendMessageToTarget(MESSAGE_RESULT_GET, result);
        //smartLinkLogger.writeCommandResult(result);
    }

    public void onSmartLinkDisconnected(SmartLinkOperationResult result)
    {
        sendMessageToTarget(MESSAGE_DISCONNECTED, result);
    }

    public void onSmartLinkConnected(SmartLinkOperationResult result)
    {
        sendMessageToTarget(MESSAGE_CONNECTED, result);
    }

    public void onSmartLinkUnProcessedResponse(SmartLinkOperationResult result)
    {
        sendMessageToTarget(MESSAGE_UNPROCESSED_RESULT_GET, result);
        smartLinkLogger.writeMessage("Unprocessed result found: \n " + result.ToString());
    }
}
}