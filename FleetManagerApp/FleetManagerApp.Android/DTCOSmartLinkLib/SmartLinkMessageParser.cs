using System;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 10.03.17
    /// </summary>
    public class SmartLinkMessageParser
    {
        protected SmartLinkMessage mMessage;

        public SmartLinkMessageParser(SmartLinkMessage msg)
        {
            mMessage = msg;
        }

        public SmartLinkOperationResult generateResult()
        {
            SmartLinkOperationResult result = new SmartLinkOperationResult();
            try
            {
                result.setData((Object)mMessage.toJSON());
            }
            catch (JSONException e)
            {
                result.setError(e.getLocalizedMessage());
            }
            return result;
        }
    }
}