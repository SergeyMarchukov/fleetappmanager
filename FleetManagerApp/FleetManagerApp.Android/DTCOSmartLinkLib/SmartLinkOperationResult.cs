using FleetManagerApp.Extensions;
using FleetManagerApp.Droid.DTCOSmartLinkLib.Commands;
using System;
using System.Text;

namespace FleetManagerApp.Droid.DTCOSmartLinkLib
{
    /// <summary>
    /// Created by Sergey M. 10.03.17
    /// </summary>
    public class SmartLinkOperationResult
    {
        public String message = "";
        public SmartLinkErrorCode error_code = SmartLinkErrorCode.NO_ERROR;
        public SmartLinkNegativeResponse? negativeResponse = null;
        private Object mData = new Object();
        public SmartLinkCmd cmd;

        public static SmartLinkOperationResult FactoryError()
        {
            return new SmartLinkOperationResult().setError(SmartLinkErrorCode.GENERAL_ERROR);
        }

        public static SmartLinkOperationResult FactoryError(String m)
        {
            return new SmartLinkOperationResult().setError(m);
        }

        public static SmartLinkOperationResult FactoryError(String m, SmartLinkErrorCode c)
        {
            return new SmartLinkOperationResult().setError(m, c);
        }

        public static SmartLinkOperationResult FactoryError(SmartLinkErrorCode c)
        {
            return new SmartLinkOperationResult().setError(c);
        }
        public static SmartLinkOperationResult Factory()
        {
            return new SmartLinkOperationResult();
        }

        public static SmartLinkOperationResult Factory(Object d)
        {
            return new SmartLinkOperationResult().setData(d);
        }
        public SmartLinkOperationResult setError(String m)
        {
            error_code = SmartLinkErrorCode.GENERAL_ERROR;
            message = m;
            return this;
        }

        public SmartLinkOperationResult setError(String m, SmartLinkErrorCode code)
        {
            error_code = code;
            message = m;
            return this;
        }

        public SmartLinkOperationResult setError(SmartLinkErrorCode code)
        {
            error_code = code;
            message = code.GetDescription();
            return this;
        }

        public Object getData()
        {
            return mData;
        }


        public SmartLinkOperationResult setData(Object data)
        {
            if (data != null)
            {
                mData = data;
            }

            return this;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Format("Error: %s,message %s ", error_code.ToString(), message));
            if (negativeResponse != null)
            {
                sb.Append((String.Format("negative response code %s(%02X)", negativeResponse.ToString(), negativeResponse.Value)));
            }
            //if (mData instanceof JSONObject){                                 // TODO
            //    sb.append(String.format("\ndata: %s", mData.toString()));
            //}

            return sb.ToString();
        }

        public bool is_ok()
        {
            return error_code == SmartLinkErrorCode.NO_ERROR;
        }
    }
}