using Android.Util;
using FleetManagerApp.Tool;

[assembly: Xamarin.Forms.Dependency(typeof(FleetManagerApp.Droid.Tool.Logger))]
namespace FleetManagerApp.Droid.Tool
{
    public class Logger : ILogger
    {
        private readonly string Tag = "DEVELOPMENT";
        private readonly string ErrorMarker = "[########]";
        private readonly string InfoMarker = "[++++++++]";

        public void Error(string msg)
        {
            Log.Error(Tag, string.Format("{0} {1}", ErrorMarker, msg));
        }

        public void Info(string msg)
        {
            Log.Info(Tag, string.Format("{0} {1}", InfoMarker, msg));
        }
    }
}